# Check that this setup script has been sourced
if [ "$BASH_SOURCE" = "$0" ]; then
  echo "ERROR! The compiler setup script was not sourced?" > /dev/stderr
  exit 1
elif [ "$BASH_SOURCE" = "" ]; then
  echo "ERROR! The compiler setup script was not sourced from bash?" > /dev/stderr
  return 1
fi

# Parse command line arguments
if [ "$1" == "" ] || [ "$2" != "" ]; then
  echo "Usage: . $BASH_SOURCE <BINARY_TAG>"
  return 1
fi
bintag=$1

# Split BINARY_TAG assuming that it is of the form "arch-os-comp-bld"
a=(${bintag//-/ })
if [ "${#a[@]}" != "4" ]; then
  echo "WARNING! BINARY_TAG '$bintag' is not of the form arch-os-comp-bld"
  return 1
fi
arch=${a[0]}
os=${a[1]}
comp=${a[2]}

# Extract compiler flavor and version
for comfla in clang gcc icc; do 
  comvers=${comp#$comfla}; if [ "$comvers" != "$comp" ]; then break; fi
done
if [ "$comvers" == "$comp" ]; then 
  echo "WARNING! Compiler '$comp' is not of a known flavor"
  return 1
elif [ "$comvers" == "" ]; then 
  echo "WARNING! Compiler '$comp' does not specify a version"
  return 1
fi

# Add dots between compiler version digits
comvers2=`echo $comvers | sed 's/\(.\)/\1./g'`
comvers2=${comvers2%.}

# Compiler-dependent setup (clang)
if [ "$comfla" == "clang" ]; then

  # [NB: clang is not yet available on CVMFS!]
  setup=/afs/cern.ch/sw/lcg/external/llvm/$comvers2/$arch-$os/setup.sh
  if [ -f $setup ]; then
    echo "INFO: set up $comp using $setup"
    . $setup
    CC=`which clang`; export CC
    CXX=`which clang++`; export CXX
  else
    echo "WARNING! $setup not found"
    return 1
  fi

# Compiler-dependent setup (gcc)
elif [ "$comfla" == "gcc" ]; then

  # [NB: some gcc older versions are not available on CVMFS!]
  setup=/cvmfs/sft.cern.ch/lcg/contrib/gcc/$comvers2/$arch-$os/setup.sh
  if [ ! -f $setup ]; then
    echo "WARNING! $setup not found"
    setup=/afs/cern.ch/sw/lcg/contrib/gcc/$comvers2/$arch-$os/setup.sh
  fi
  if [ -f $setup ]; then
    echo "INFO: set up $comp using $setup"
    . $setup
    CC=`which gcc`; export CC
    CXX=`which g++`; export CXX
  else
    echo "WARNING! $setup not found"
    return 1
  fi

# Compiler-dependent setup (icc)
elif [ "$comfla" == "icc" ]; then

  # [NB: icc is not yet available on CVMFS!]
  intelroot=/afs/cern.ch/sw/IntelSoftware/linux

  if [ x`env | grep INTEL_LICENSE_FILE` = "x" ] ; then
    setup=$intelroot/setup.sh 
    if [ -f $setup ]; then
      echo "INFO: set up Intel licenses using $setup"
      . $setup
    else
      echo "WARNING! $setup not found"
      return 1
    fi
  fi

  setup=$intelroot/$arch/xe20$comvers/bin/iccvars.sh
  if [ -f $setup ]; then
    echo "INFO: set up $comp using $setup"
    . $setup intel64
    CC=`which icc`; export CC
    CXX=`which icpc`; export CXX
  else
    echo "WARNING! $setup not found"
    return 1
  fi

fi

# Dump the relevant variables
###echo CC=$CC
###echo CXX=$CXX
###echo LD_LIBRARY_PATH=$LD_LIBRARY_PATH
