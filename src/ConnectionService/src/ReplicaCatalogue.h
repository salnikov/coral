#ifndef CONNECTIONSERVICE_REPLICACATALOGUE_H
#define CONNECTIONSERVICE_REPLICACATALOGUE_H 1

#include <map>
#include <vector>

#include "CoralBase/TimeStamp.h"
#include "CoralBase/../src/coral_mutex_headers.h"
#include "RelationalAccess/AccessMode.h"

#include "ConnectionParams.h"

namespace coral
{

  namespace ConnectionService
  {

    class ConnectionServiceConfiguration;

    /// Local service to resolve the logical connection
    /// and provide the list of replicas
    class ReplicaCatalogue
    {

    public:

      typedef std::chrono::system_clock::time_point PosixTime;

    public:

      /// Constructor
      ReplicaCatalogue( const ConnectionServiceConfiguration& serviceConfiguration );

      /// Destructor
      virtual ~ReplicaCatalogue();

      /// Resolves the technology specified in the connection string,
      /// checking if it is supported by an existing plug-in...
      bool isConnectionPhysical( const std::string& connectionString ) const;

      /// Returns a vector of connection strings
      /// (the replicas of the specified connection
      std::vector<ConnectionParams> getReplicas( const std::string& connectionString,
                                                 AccessMode accessMode ) const;

      /// Sets the specified connection as unavailable
      void setConnectionExcluded( const std::string& technologyName,
                                  const std::string& serviceName );

      /// Checks if the specified connection appears in the list of unavailable
      bool isConnectionExcluded( const std::string& technologyName,
                                 const std::string& serviceName ) const;

    private:

      /// The reference to the service configuration
      const ConnectionServiceConfiguration& m_serviceConfiguration;

      /// The map of (temporarely) unavailable connections
      mutable std::map<std::string,PosixTime> m_excludedConnections;

      /// Semaphore
      mutable coral::mutex m_mutexForExclusionList;

    };

  }

}

#endif
