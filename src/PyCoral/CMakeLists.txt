# Required external packages
if (LCG_python3 STREQUAL on) # CORALCOOL-2976
  find_package(PythonLibs REQUIRED 3)
else()
  find_package(PythonLibs REQUIRED)
endif()
include_directories(${PYTHON_INCLUDE_DIRS})

include(CORALModule)
  CORALModule(LIBS lcg_CoralCommon ${PYTHON_LIBRARIES} HEADERS)

# Copy and install python modules into the build and install areas
include(CORALConfigPython)
coral_install_python_modules()

#----------------------------------------------------------------------------
# PyCoral unit tests
#----------------------------------------------------------------------------

macro(pycoral_test dir)
  file(GLOB _files RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/${dir} ${dir}/*.py)
  foreach(_file ${_files})
    copy_to_build(${_file} ${dir} tests/bin)
  endforeach(_file)
endmacro()
if (LCG_python3 STREQUAL on) # CORALCOOL-2976
  pycoral_test(tests/Python3)
else()
  include(CORALConfigScripts)
  macro(pycoral_unittest dir test)
    copy_to_build(${test} tests/${dir} tests/bin)
  endmacro()
  # The 'import coral' test
  pycoral_unittest(AttributeList test_PyCoralUnit_AttributeList.py)

endif()
