#include <iostream>
#include "CoralKernel/Service.h"
#include "RelationalAccess/SchemaException.h"
#include "RelationalAccess/SessionException.h"

#include "Cursor.h"
#include "DomainProperties.h"
#include "SessionProperties.h"
#include "SQLiteStatement.h"

coral::SQLiteAccess::Cursor::Cursor( SQLiteStatement* statement,
                                     coral::AttributeList& rowBuffer )
  : m_statement( statement )
  , m_rowBuffer( rowBuffer )
  , m_started( false )
{
}


coral::SQLiteAccess::Cursor::~Cursor()
{
  this->close();
}


bool
coral::SQLiteAccess::Cursor::next()
{
  //if(!m_statement->sessionProperties()->session())
  //  throw coral::SessionException( "The cursor is no longer valid",
  //                                 "SQLiteStatement::next",
  //                                 m_statement->sessionProperties()->domainProperties().service()->name());
  if ( m_statement == 0 ) return false;
  bool result = m_statement->fetchNext();
  if ( result && !m_started ) m_started=true;  // fix bug #91028
  if ( ! result )
  {
    this->close();
    return false;
  }
  else
  {
    if( !m_statement->defineOutput(m_rowBuffer) ) {
      throw coral::QueryException("sqlite", "Could not define output","ICursor::currentRow" );
    }
  }
  return result;
}


const coral::AttributeList&
coral::SQLiteAccess::Cursor::currentRow() const
{
  //if(!m_statement->sessionProperties()->session())
  //  throw coral::SessionException( "The cursor is no longer valid",
  //                                 "SQLiteStatement::currentRow",
  //                                 m_statement->sessionProperties()->domainProperties().service()->name());
  if ( !m_started ) // fix bug #91028
    throw coral::QueryException( "SQLite",
                                 "Cursor loop has not started yet",
                                 "ICursor::currentRow()" );
  return m_rowBuffer;
}


void
coral::SQLiteAccess::Cursor::close()
{
  if ( m_statement != 0 )
  {
    delete m_statement;
    m_statement = 0;
  }
}
