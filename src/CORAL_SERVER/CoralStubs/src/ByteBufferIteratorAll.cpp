// Include files
#include "CoralMonitor/ScopedTimer.h"
#include "CoralServerBase/ConnectionProperties.h"
#include "CoralServerBase/RequestProperties.h"
#include "CoralServerBase/../src/debug2936.h"

// Local include files
#include "ByteBufferIteratorAll.h"

#define LOGGER_NAME "CoralStubs::ByteBufferIteratorAll"
#include "logger.h"

// Namespace
using namespace coral::CoralStubs;

//-----------------------------------------------------------------------------

ByteBufferIteratorAll::ByteBufferIteratorAll( std::unique_ptr<IRowIterator> rowi,
                                              CALOpcode opcode,
                                              bool cacheable,
                                              bool proxy,
                                              bool isempty,
                                              std::unique_ptr<AttributeList> rowBuffer )
  : m_swi(opcode, cacheable, true)
  , m_rowi( std::move(rowi) )
  , m_rowBuffer( std::move(rowBuffer) )
  , m_isempty( isempty )
  , m_islast( false )
  , m_lastbuffer( false )
  , m_nextBufferCount( 0 )
{
  if ( debug2936 ) std::cout << "__Enter BBIAll::ctor" << std::endl; // debug CORALCOOL-2936
  m_swi.setProxy( proxy );
  if(isempty)
  {
    // this may throw, exception will be handled by ServerStub
    if(m_rowi->nextRow())
    {
      m_swi.append( true );
      const AttributeList& attr01 = m_rowi->currentRow();
      //first send the attribute list
      m_swi.appendE( attr01 );
      m_swi.useRowStructure(attr01);
      //write only the raw data
      m_swi.appendD(attr01);
    }
    else
    {
      //send termination
      m_swi.append( false );
      m_swi.flush();
      //set as last block
      m_islast = true;
    }
  }
  else
  {
    m_swi.useRowStructure(*m_rowBuffer);
  }
  if ( debug2936 ) std::cout << "__Exit BBIAll::ctor" << std::endl; // debug CORALCOOL-2936
}

//-----------------------------------------------------------------------------

ByteBufferIteratorAll::~ByteBufferIteratorAll()
{
}

//-----------------------------------------------------------------------------

void
ByteBufferIteratorAll::fillBuffer()
{
  if ( debug2936 ) std::cout << "__Enter BBIAll::fillBuffer" << std::endl; // debug CORALCOOL-2936
  try
  {
    while(!m_swi.nextBuffer())
    {
      //std::cout << "try to get the next row from row iterator ...";
      bool newrow = m_rowi->nextRow();
      //std::cout << "done [" << newrow << "]" << std::endl;
      if(newrow)
      {
        if(m_isempty)
        {
          const AttributeList& attr02 = m_rowi->currentRow();
          m_swi.append( true );
          //write only the raw data
          m_swi.appendD(attr02);
        }
        else
        {
          m_swi.append( true );
          //write only the raw data
          m_swi.appendD(*m_rowBuffer);
        }
      }
      else
      {
        //set the terminal
        m_swi.append( false );
        //flush the last time
        //now we should have something in the buffer
        //will return in the next loop
        m_swi.flush();
        //return as is last
        m_islast = true;
      }
    }
    //check if it was the last
    //only check if the m_islast set to true
    //otherwise it is always last as long there is only one entry in the buffer
    //FIXME fix that on the swi
    m_lastbuffer = m_swi.isLastBuffer() && m_islast;
    if ( debug2936 ) std::cout << "__Exit BBIAll::fillBuffer" << std::endl; // debug CORALCOOL-2936
  }
  catch( std::exception& e )
  {
    // MOST LIKELY, THIS _IS_ CORALCOOL-2936: THIS EXCEPTION IS NOT HANDLED!
    if ( debug2936 ) std::cout << "__Exception caught in BBIAll::fillBuffer: " << e.what() << std::endl; // debug CORALCOOL-2936
    throw;
  }
}

//-----------------------------------------------------------------------------

bool
ByteBufferIteratorAll::nextBuffer()
{
  SCOPED_TIMER( "ServerStub::ByteBufferIteratorAll::nextBuffer" );
  if(!m_islast)
  {
    if(m_swi.nextBuffer())
    {
      ++ m_nextBufferCount;
      return true;
    }
    else
    {
      try
      {
        fillBuffer();
      }
      catch ( coral::Exception& e )
      {
        if (m_nextBufferCount != 0) {
          // if we have sent some data already there is no easy way to append
          // exception to the message, just crash hard (this will likely cause
          // socket close).
          throw;
        }

        logger << Error << "Caught coral::Exception: '" << e.what() << "'" << endlog;
        // replace buffer with exception packet
        m_swi.appendException(0x00, 0x02, e.what());
        m_swi.nextBuffer();
        m_lastbuffer = m_islast = true;

      }
      ++ m_nextBufferCount;
      return true;
    }
  }
  else
  {
    if(m_swi.nextBuffer())
    {
      m_lastbuffer = m_swi.isLastBuffer();
      ++ m_nextBufferCount;
      return true;
    }
  }
  return false;
}

//-----------------------------------------------------------------------------

bool
ByteBufferIteratorAll::isLastBuffer() const
{
  return m_lastbuffer;
}

//-----------------------------------------------------------------------------

const coral::ByteBuffer&
ByteBufferIteratorAll::currentBuffer() const
{
  return m_swi.currentBuffer();
}

//-----------------------------------------------------------------------------
