#ifndef CORALSTUBS_SEGMENTWRITERITERATOR_H
#define CORALSTUBS_SEGMENTWRITERITERATOR_H

// Include files
#include <list>
#include <map>
#include <set>
#include <string>
#include "CoralBase/Attribute.h"
#include "CoralBase/AttributeList.h"
#include "CoralBase/Blob.h"
#include "CoralBase/Date.h"
#include "CoralBase/TimeStamp.h"
#include "CoralServerBase/ByteBuffer.h"
#include "CoralServerBase/CALOpcode.h"
#include "CoralServerBase/CTLPacketHeader.h"
#include "CoralServerBase/IByteBufferIterator.h"
#include "CoralServerBase/QueryDefinition.h"
#include "RelationalAccess/IQueryDefinition.h"
#include "RelationalAccess/TableDescription.h"

// Local include files
#include "CppTypes.h"

namespace coral
{

  namespace CoralStubs
  {

    /** @class SegmentWriteIterator
     *
     *  This class contains a buffer mechanism to append
     *  several types.
     *  The basic buffer used inside is static, and can
     *  be retrieved over the currentBuffer() method.
     *  Overflows of the static buffer are handled by adding
     *  this one to a list. Calling nextBuffer() will first clear
     *  the list before continue appending types.
     *
     *  @author Alexander Kalkhof
     *  @date   2009-03-31
     *///

    class SegmentWriterIterator : public IByteBufferIterator
    {
    public:
      //constructor
      SegmentWriterIterator(CALOpcode opcode,
                            bool cacheable,
                            bool reply,
                            size_t buffersize = (CTLPACKET_MAX_SIZE - CTLPACKET_HEADER_SIZE) );
      //destructor
      ~SegmentWriterIterator();
      //get the next buffer
      bool nextBuffer() override;
      //get the current buffer
      const ByteBuffer& currentBuffer() const override;
      //check if we have the last buffer
      bool isLastBuffer() const override;
      //writes the current buffer to the iterator
      //the header will be added to each buffer inside the iterator
      void flush();
      //change the current buffer to an exception
      //the header will be written and flush is called
      size_t written();

      void setProxy(bool proxy);

      void appendException(const CALOpcode,
                           const CALOpcode,
                           const std::string message,
                           const std::string method,
                           const std::string from);

      // just as above but `method` and `from` parts are extracted the
      // full message which is expected to be a string representation of
      // coral::Exception type.
      void appendException(const CALOpcode,
                           const CALOpcode,
                           const std::string message);

      void append(const bool);

      void append(const uint16_t);

      void append(const uint32_t);

      void append(const uint64_t);

      void append(const uint128_t&);

      // add string, length is specified as 16-bit number
      void append16(const std::string&);

      // add string, length is specified as 32-bit number
      void append32(const std::string&);

      void append(const Blob&);

      void append(const coral::Date&);

      void append(const coral::TimeStamp&);

      void append(const CALOpcode);

      void append(const std::vector<std::string>&);

      void append(const std::set<std::string>&);

      void append(const std::vector< std::pair<std::string, std::string> >&);

      void append(const std::map< std::string, std::string >&);

      void append(const TableDescription&);

      void append(const std::map< std::string, TableDescription >&);

      void append(const QueryDefinition&);

      void append(const IQueryDefinition::SetOperation&);

      /**
       *  Serialize the structure of the data.
       *
       *  Only stores names and types of attributes, not the attribute data.
       */
      void appendV(const AttributeList& data);

      /**
       *  Serialize the structure of the data.
       *
       *  Only stores names and types of attributes, not the attribute data.
       *  Usually called once before one or many appendD() calls.
       */
      void appendE(const AttributeList& data);

      /**
       *  Remember field structure of the given data row.
       *
       *  This is an optimization feature that helps in serialization of many rows
       *  at a time. Clients call useRowstructure() once and then repeatedly call
       *  appendD() method which uses info stored internally by useRowstructure().
       */
      void useRowStructure(const AttributeList& row);

      /**
       *  Serialize single row.
       *
       *  Before calling this method client has to call useRowstructure(). It
       *  is assumed that all rows passed to appendD() have exactly the same
       *  structure (as the one passed to useRowstructure())
       */
      void appendD(const AttributeList& row);

      void print() const;

    private:

      /// Copy constructor is private (fix Coverity MISSING_COPY)
      SegmentWriterIterator( const SegmentWriterIterator& rhs );

      /// Assignment operator is private (fix Coverity MISSING_ASSIGN)
      SegmentWriterIterator& operator=( const SegmentWriterIterator& rhs );

      void extend();

      void newbuffer();

      void append(const std::string&, size_t);

      template<class T>
      void appendAttribute(const coral::CALOpcode&,
                           const bool,
                           const Attribute&);

      template<class T>
      void appendAttributeN(const coral::CALOpcode&,
                            const bool,
                            const Attribute&);

    private:

      typedef void (SegmentWriterIterator::*WriteAttributeFunction)(const void*);

      // Methods to add attribute data, take pointer to data as argument
      void writeAttribute_bool(const void*);
      void writeAttribute_char(const void*);
      void writeAttribute_short(const void*);
      void writeAttribute_uint(const void*);
      void writeAttribute_ul(const void*);
      void writeAttribute_ull(const void*);
      void writeAttribute_float(const void*);
      void writeAttribute_double(const void*);
      void writeAttribute_longdouble(const void*);
      void writeAttribute_string(const void*);
      void writeAttribute_blob(const void*);
      void writeAttribute_date(const void*);
      void writeAttribute_time(const void*);

      // appendN() methods are used for storing "native" non-portable C++
      // data types, this is used with Attributes that are usually defined
      // in terms of those native types.
      void appendN(const unsigned int);
      void appendN(const unsigned long);
      void appendN(const unsigned long long);

    private:

      unsigned char* m_bufferpos;
      unsigned char* m_bufferend;
      std::list<ByteBuffer*> m_buffers;
      ByteBuffer* m_currentbuffer;
      ByteBuffer* m_iterbuffer;
      CALOpcode m_opcode;
      bool m_cacheable;
      bool m_proxy;
      bool m_reply;
      int m_nocache;
      size_t m_buffersize;
      std::vector<WriteAttributeFunction> m_rowStructure;

    };

    // Declaration of template specialization must be in namespace scope
    template<>
    void
    SegmentWriterIterator::appendAttribute<std::string>( const CALOpcode& opcode, const bool isnull, const Attribute& attr );

  }

}

#endif
