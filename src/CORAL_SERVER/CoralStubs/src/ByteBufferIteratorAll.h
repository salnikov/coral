#ifndef CORAL_CORALSTUBS_BYTEBUFFERITERATORALL_H
#define CORAL_CORALSTUBS_BYTEBUFFERITERATORALL_H

#include <memory>

//CoralServer includes
#include "CoralServerBase/CALOpcode.h"
#include "CoralServerBase/IByteBufferIterator.h"
#include "CoralServerBase/IRowIterator.h"

//Coral includes
#include "CoralBase/AttributeList.h"

//CoralServerStubs includes
#include "SegmentWriterIterator.h"

namespace coral {

  // Forward declaration
  struct RequestProperties;
  struct ConnectionProperties;

  namespace CoralStubs {

    class ByteBufferIteratorAll : public IByteBufferIterator {
    public:

      /**
       * @param rowi: result row iterator
       * @param cacheable: cacheable flag to set in packet header
       * @param proxy: proxy flag to set in packet header
       * @param isempty: if true then query does not define row buffer and
       *                 rowBuffer argument is not used
       * @param rowBuffer: row buffer used for queries if isempty is false
       */
      ByteBufferIteratorAll(std::unique_ptr<IRowIterator> rowi,
                            CALOpcode opcode,
                            bool cacheable,
                            bool proxy,
                            bool isempty,
                            std::unique_ptr<AttributeList> rowBuffer);

      ~ByteBufferIteratorAll();

      bool nextBuffer() override;

      bool isLastBuffer() const override;

      const ByteBuffer& currentBuffer() const override;

    private:

      /// Copy constructor is private (fix Coverity MISSING_COPY)
      ByteBufferIteratorAll( const ByteBufferIteratorAll& rhs );

      /// Assignment operator is private (fix Coverity MISSING_ASSIGN)
      ByteBufferIteratorAll& operator=( const ByteBufferIteratorAll& rhs );

      void fillBuffer();

      SegmentWriterIterator m_swi;

      std::unique_ptr<IRowIterator> m_rowi;

      std::unique_ptr<AttributeList> m_rowBuffer;

      bool m_isempty;

      bool m_islast;     // set to true after last row is read from row iterator

      bool m_lastbuffer;

      size_t m_nextBufferCount; // how many times nextBuffer() returned true

    };

  }

}

#endif
