#ifndef CORALSTUBS_DUMMYFACADE_H
#define CORALSTUBS_DUMMYFACADE_H 1

// Include files
#include <string>
#include "CoralServerBase/ICoralFacade.h"
#include "RelationalAccess/AccessMode.h"

namespace coral
{

  namespace CoralStubs
  {

    /** @class DummyFacade
     *
     *  An inheritance of the ICoralFacade used for testing and debug mode only
     *
     *  This is realy only a tests class
     *
     *  @author Alexander Kalkhof
     *  @date   2009-01-29
     *///

    class DummyFacade : public ICoralFacade
    {
    public:

      // Constructor
      DummyFacade();

      // Virtual destructor.
      virtual ~DummyFacade();

      void setCertificateData( const coral::ICertificateData* cert ) override;

      Token connect( const std::string& dbUrl,
              const coral::AccessMode mode,
              bool& fromProxy ) const override;

      void releaseSession( coral::Token sessionID ) const override;

      void startTransaction( coral::Token sessionID,
              bool readOnly = false ) const override;

      void commitTransaction( coral::Token sessionID ) const override;

      void rollbackTransaction( coral::Token sessionID ) const override;

      const std::vector<std::string> fetchSessionProperties( Token sessionID ) const override;

      const std::set<std::string> listTables( Token sessionID,
              const std::string& schemaName ) const override;

      bool existsTable( Token sessionID,
              const std::string& schemaName,
              const std::string& tableName ) const override;

      const TableDescription fetchTableDescription( Token sessionID,
              const std::string& schemaName,
              const std::string& tableName ) const override;

      const std::set<std::string> listViews( Token sessionID,
              const std::string& schemaName ) const override;

      bool existsView( Token sessionID,
              const std::string& schemaName,
              const std::string& viewName ) const override;

      const std::pair<TableDescription,std::string>
      fetchViewDescription( Token sessionID,
              const std::string& schemaName,
              const std::string& viewName ) const override;

      IRowIteratorPtr fetchRows( Token sessionID,
              const QueryDefinition& qd,
              AttributeList* pOutputBuffer,
              unsigned int rowCacheSize,
              unsigned int memoryCacheSize ) const override;

      IRowIteratorPtr fetchRows( Token sessionID,
              const QueryDefinition& qd,
              const std::map< std::string, std::string > outputTypes,
              unsigned int rowCacheSize,
              unsigned int memoryCacheSize ) const override;

      IRowIteratorPtr fetchAllRows( Token sessionID,
              const QueryDefinition& qd,
              AttributeList* rowBuffer ) const override;

      IRowIteratorPtr fetchAllRows( Token sessionID,
              const QueryDefinition& qd,
              const std::map< std::string,
              std::string > outputTypes ) const override;

      void callProcedure( Token sessionID,
              const std::string& schemaName,
              const std::string& procedureName,
              const coral::AttributeList& inputArguments ) const override;

      /*
      int deleteTableRows( Token sessionID,
              const std::string& schemaName,
              const std::string& tableName,
              const std::string& whereClause,
              const std::string& whereData ) const override;

      const std::string formatRowBufferAsString( Token sessionID,
              const std::string& schemaName,
              const std::string& tableName ) const override;

      void insertRowAsString( Token sessionID,
              const std::string& schemaName,
              const std::string& tableName,
              const std::string& rowBufferAS ) const override;

      Token bulkInsertAsString( Token sessionID,
              const std::string& schemaName,
              const std::string& tableName,
              const std::string& rowBufferAS,
              int rowCacheSizeDb ) const override;

      void releaseBulkOp( Token bulkOpID ) const override;

      void processRows( Token bulkOpID,
              const std::vector<coral::AttributeList>& rowsAS ) const override;

      void flush( Token bulkOpID ) const;
      */

    private:

    };

  }

}
#endif
