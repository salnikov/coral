#ifndef CORALSOCKETS_POLL_H
#define CORALSOCKETS_POLL_H 1

#include <memory>

#include "ISocket.h"

namespace coral {

  namespace CoralSockets {

    // forward definition
    class PollFDs;

    class Poll  {
    public:
      enum PMode {
        P_READ,
        P_WRITE,
        P_READ_WRITE,
        P_NONE        // P_NONE means do not poll
      };


      /// a class which polls the state of several ISockets,
      /// most of the methods are not thread safe except for ones
      /// marked explicitly as thread safe
      Poll(unsigned int maxFDs=128);

      virtual ~Poll();

      /// add a socket to poll call, this invalidates iteration
      void addSocket( const ISocketPtr& Socket, PMode mode );

      /// remove a socket from the poll call, this invalidates iteration
      void removeSocket( const ISocketPtr& Socket );

      /// update polling mode
      void updateSocket( const ISocketPtr& Socket, PMode mode );

      /// polls all previously added sockets and returns
      /// the number of sockets which are ready for the
      /// requested operations.
      /// Iterate over the sockets with getNextReadySocket.
      int poll( int timeout );

      /// pre-emptively stop polling, this method is thread safe  (and it is
      /// supposed to be called from thread other than one calling poll())
      void stopPoll();

      /// returns the next socket which is ready for
      /// an operation.
      /// Throws if there is no next socket.
      const ISocketPtr& getNextReadySocket();

      bool currSocketClosed();

    protected:

      /// wrapper class to handle array of struct pollfd
      std::unique_ptr<PollFDs> m_pollFD;

      /// current socket (for getNextReadySocket() )
      int m_currentSocket;

      /// how many sockets are ready for an operation
      int m_maxReadySockets;

      /// pipe for stop signaling
      int m_pipefd[2];

    };
  }
}

#endif // CORALSOCKETS_TCPSOCKET_H
