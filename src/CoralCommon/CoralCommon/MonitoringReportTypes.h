// -*- C++ -*-
#ifndef CORAL_MONITOR_REPORT_TYPES_H
#define CORAL_MONITOR_REPORT_TYPES_H 1

namespace coral
{
  namespace monitor
  {
    enum ReportType
      {
        Csv = 1,
        Xml,
        SQLite
      };
  }
}

#endif // CORAL_MONITOR_REPORT_TYPES_H
