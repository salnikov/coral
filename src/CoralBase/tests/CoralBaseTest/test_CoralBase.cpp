// Include files
#include <cstdlib>
#include <iostream>
#include "CoralBase/Attribute.h"
#include "CoralBase/AttributeList.h"
#include "CoralBase/AttributeListException.h"
#include "CoralBase/AttributeListSpecification.h"
#include "CoralBase/AttributeSpecification.h"
#include "CoralBase/Date.h"
#include "CoralBase/Exception.h"
#include "CoralBase/TimeStamp.h"
#include "CoralBase/../tests/Common/CoralCppUnitTest.h"

// Debug printouts for CORALCOOL-2904?
//const bool debug2904=true;
const bool debug2904=false;

// Forward declaration (for easier indentation)
namespace coral
{
  class CoralBaseTest;
}

// The test class
class coral::CoralBaseTest : public coral::CoralCppUnitTest
{

  CPPUNIT_TEST_SUITE( CoralBaseTest );

  // The following tests were originally based on stdout comparison
  // (which has not yet been ported to CppUnit)
  CPPUNIT_TEST( test_ALS_alsFunctionality );
  CPPUNIT_TEST( test_AL_simpleAtLi );
  CPPUNIT_TEST( test_AL_atLiWithSpec );
  CPPUNIT_TEST( test_AL_atLiWithSharedSpec );
  CPPUNIT_TEST( test_AL_atLiWithSharedSpec1 );
  CPPUNIT_TEST( test_AL_atLiWithSharedSpec2 );
  CPPUNIT_TEST( test_AL_atLiMerge1 );
  CPPUNIT_TEST( test_AL_atLiMerge2 );
  CPPUNIT_TEST( test_AL_atLiMerge3 );
  CPPUNIT_TEST( test_AL_atLiWithSharedSpec1Count );
  CPPUNIT_TEST( test_AL_atLiWithSharedSpecNastyUser );
  CPPUNIT_TEST( test_AL_atLiBound );
  CPPUNIT_TEST( test_AL_atLiShared );
  CPPUNIT_TEST( test_attributeReadWrite );
  CPPUNIT_TEST( test_attributeReadWrite_testSharingLoop );

  // These tests are about char, they do not test CORAL
  CPPUNIT_TEST( test_char );

  CPPUNIT_TEST_SUITE_END();

public:

  void setUp() {}

  void tearDown() {}

  // ------------------------------------------------------

  void
  test_char()
  {
    std::cout << std::endl;
    int im( -100 );
    int ip( +156 );

    // Can convert any int to char (or schar or uchar)
    CPPUNIT_ASSERT_MESSAGE( "(char)-100 == (char)156", (char)im == (char)ip );
    CPPUNIT_ASSERT_MESSAGE( "(char)(156+256*ANY) == (char)156",
                            (char)ip == (char)(ip+256*12345) );
    CPPUNIT_ASSERT_MESSAGE( "(schar)-100 == (schar)156",
                            (signed char)im == (signed char)ip );
    CPPUNIT_ASSERT_MESSAGE( "(uchar)-100 == (uchar)156",
                            (unsigned char)im == (unsigned char)ip );

    // Converting char to int is not portable (different on x86 and ARM)
#ifndef __CHAR_UNSIGNED__ // char is signed (better than #ifndef __ARM_ARCH)
    CPPUNIT_ASSERT_MESSAGE( "-100 => char => -100", (int)((char)im) == im );
    CPPUNIT_ASSERT_MESSAGE( "+156 => char => -100", (int)((char)ip) == im );
#else // char is unsigned
    CPPUNIT_ASSERT_MESSAGE( "-100 => char => +156", (int)((char)im) == ip );
    CPPUNIT_ASSERT_MESSAGE( "+156 => char => +156", (int)((char)ip) == ip );
#endif

    // Converting signed/unsigned char to int is portable
    CPPUNIT_ASSERT_MESSAGE( "-100 => schar => -100",
                            (int)((signed char)im) == im );
    CPPUNIT_ASSERT_MESSAGE( "+156 => schar => -100",
                            (int)((signed char)ip) == im );
    CPPUNIT_ASSERT_MESSAGE( "-100 => uchar => +156",
                            (int)((unsigned char)im) == ip );
    CPPUNIT_ASSERT_MESSAGE( "+156 => uchar => +156",
                            (int)((unsigned char)ip) == ip );

    // Converting char to int passing through uchar/schar is portable
    // Converting char to int reinterpreting as uchar/schar is portable
    char cp = (char)ip;
    char cm = (char)im;
    CPPUNIT_ASSERT_MESSAGE( "cm == cp", cm == cp );
    CPPUNIT_ASSERT_MESSAGE( "-100 => char => schar => -100",
                            (int)((signed char)cm) == im );
    CPPUNIT_ASSERT_MESSAGE( "+156 => char => schar => -100",
                            (int)((signed char)cp) == im );
    CPPUNIT_ASSERT_MESSAGE( "-100 => char => uchar => +156",
                            (int)((unsigned char)cm) == ip );
    CPPUNIT_ASSERT_MESSAGE( "+156 => char => uchar => +156",
                            (int)((unsigned char)cp) == ip );
    CPPUNIT_ASSERT_MESSAGE( "-100 => char => reint<schar> => -100",
                            (int)(reinterpret_cast<signed char&>(cm)) == im );
    CPPUNIT_ASSERT_MESSAGE( "+156 => char => reint<schar> => -100",
                            (int)(reinterpret_cast<signed char&>(cp)) == im );
    CPPUNIT_ASSERT_MESSAGE( "-100 => char => reint<uchar> => +156",
                            (int)(reinterpret_cast<unsigned char&>(cm)) == ip );
    CPPUNIT_ASSERT_MESSAGE( "+156 => char => reint<uchar> => +156",
                            (int)(reinterpret_cast<unsigned char&>(cp)) == ip );

    // Casting schar to uchar and viceversa is also ok...
    signed char sc = (signed char)im;
    unsigned char uc = (unsigned char)im;
    signed char scm = im;
    unsigned char ucm = im;
    signed char scp = ip;
    unsigned char ucp = ip;
    CPPUNIT_ASSERT_MESSAGE( "(schar)-100 == (schar)156",
                            sc == (signed char)ip );
    CPPUNIT_ASSERT_MESSAGE( "(uchar)-100 == (uchar)156",
                            uc == (unsigned char)ip );
    CPPUNIT_ASSERT_MESSAGE( "sc == (signed char)uc",
                            sc == (signed char)uc );
    CPPUNIT_ASSERT_MESSAGE( "uc == (unsigned char)sc",
                            uc == (unsigned char)sc );

    // ... but assigning directly without cast is different?...
    std::cout << im << "," << (int)sc << std::endl;
    std::cout << im << "," << (int)scp << std::endl;
    std::cout << im << "," << (int)scm << std::endl;
    std::cout << im << "," << (int)uc << std::endl;
    std::cout << im << "," << (int)ucp << std::endl;
    std::cout << im << "," << (int)ucm << std::endl;


    std::cout << std::endl;
  }

  // ------------------------------------------------------

  void
  test_ALS_alsFunctionality()
  {
    std::cout << std::endl;
    coral::AttributeListSpecification* spec = new coral::AttributeListSpecification;
    spec->extend<double>( "x" );
    spec->extend( "y", typeid(float) );
    spec->extend( "z", "int" );
    for ( coral::AttributeListSpecification::const_iterator iSpec = spec->begin(); iSpec != spec->end(); ++iSpec )
    {
      std::cout << iSpec->name() << " : " << iSpec->typeName() << std::endl;
    }
    const coral::AttributeSpecification& atSpec = spec->specificationForAttribute( spec->index( "y" ) );
    std::cout << "Type for \"y\" is \"" << atSpec.typeName() << "\"" << std::endl;
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS contains x", spec->exists("x"), true );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS contains w", spec->exists("w"), false );
    spec->release();
    std::cout << std::endl;
  }

  // ------------------------------------------------------

  void
  test_AL_simpleAtLi()
  {
    std::cout << std::endl;
    coral::AttributeList atlist;
    atlist.extend<double>( "x" );
    atlist.extend( "y", "float" );
    atlist.extend( "z", typeid(int) );
    atlist.extend( "d", typeid(coral::Date) );
    atlist.extend( "t", typeid(coral::TimeStamp) );
    atlist.extend( "ll", "long long" );
    atlist["y"].data<float>() = 2;
    atlist[2].data<int>() = 3;
    atlist.begin()->data<double>() = 1;
    atlist[3].data<coral::Date>() = coral::Date(); // UTC (task #51263)
    atlist[4].setValue( coral::TimeStamp::now() );
    atlist[5].data<long long>() = 12345678900LL;
    atlist.toOutputStream( std::cout );
    // Add test for task #20089
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL contains x", atlist.exists("x"), true );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL contains w", atlist.exists("w"), false );
    // Add test for CORALCOOL-1254 and CORALCOOL-1281
    const coral::AttributeListSpecification& spec = atlist.specification();
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS size", spec.size(), 6UL );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS x", spec.index("x"), 0 );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS y", spec.index("y"), 1 );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS z", spec.index("z"), 2 );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS d", spec.index("d"), 3 );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS t", spec.index("t"), 4 );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS ll", spec.index("ll"), 5 );
    CPPUNIT_ASSERT_MESSAGE( "ALS 0", spec[0].type() == typeid(double) );
    CPPUNIT_ASSERT_MESSAGE( "ALS 1", spec[1].type() == typeid(float) );
    CPPUNIT_ASSERT_MESSAGE( "ALS 2", spec[2].type() == typeid(int) );
    CPPUNIT_ASSERT_MESSAGE( "ALS 3", spec[3].type() == typeid(coral::Date) );
    CPPUNIT_ASSERT_MESSAGE( "ALS 4", spec[4].type() == typeid(coral::TimeStamp) );
    CPPUNIT_ASSERT_MESSAGE( "ALS 5", spec[5].type() == typeid(long long) );
    std::cout << std::endl;
  }

  // ------------------------------------------------------

  void
  test_AL_atLiWithSpec() // see also CORALCOOL-2904
  {
    std::cout << std::endl;
    coral::AttributeListSpecification* spec = new coral::AttributeListSpecification;
    spec->extend<double>( "x" );
    spec->extend( "y", "float" );
    spec->extend( "z", typeid(int) );
    coral::AttributeList atlist1( *spec ); // shared=false, AL makes its own copy!
    atlist1.begin()->data<double>() = 1;
    atlist1["y"].data<float>() = 2;
    atlist1[2].data<int>() = 3;
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS size (1)", spec->size(), 3UL );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL1 size (1)", atlist1.size(), 3UL );
    spec->extend<unsigned int>( "i" );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS size (2)", spec->size(), 4UL );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL1 size (2)", atlist1.size(), 3UL ); // OK, AL1 has its own separate spec copy
    coral::AttributeList atlist2( *spec );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL2 size (2)", atlist2.size(), 4UL );
    atlist2.begin()->data<double>() = 10;
    atlist2["y"].data<float>() = 20;
    atlist2[2].data<int>() = 30;
    atlist2[3].data<unsigned int>() = 40;
    // BEGIN WEIRDNESS TESTS
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS[2] name", (*spec)[2].name(), std::string("z") );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL1[2] name", atlist1[2].specification().name(), std::string("z") );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS[3] name", (*spec)[3].name(), std::string("i") );
    atlist1.extend<bool>( "j" );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL1 size (3)", atlist1.size(), 4UL );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL1[3] name", atlist1[3].specification().name(), std::string("j") );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS size (3)", spec->size(), 4UL ); // OK, no change
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL1 spec size (3)", atlist1.specification().size(), 4UL ); // OK, ALS has its own copy
    // END WEIRDNESS TESTS
    spec->release();
    atlist1.toOutputStream( std::cout );
    std::cout << std::endl;
    atlist2.toOutputStream( std::cout );
    std::cout << std::endl;
  }

  // ------------------------------------------------------

  void
  test_AL_atLiWithSharedSpec() // see also CORALCOOL-2904
  {
    std::cout << std::endl;
    // ALS
    coral::AttributeListSpecification* spec = new coral::AttributeListSpecification; // count=1
    spec->extend<double>( "x" );
    spec->extend( "y", "float" );
    spec->extend( "z", typeid(int) );
    // AL1
    coral::AttributeList atlist1( *spec, true ); // shared=true, spec count=2
    atlist1.begin()->data<double>() = 1;
    atlist1["y"].data<float>() = 2;
    atlist1[2].data<int>() = 3;
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS size (1)", spec->size(), 3UL );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL1 size (1)", atlist1.size(), 3UL );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL1 spec (1)", &(atlist1.specification()), (const AttributeListSpecification*)spec );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL1 specs [0] (1)", &(atlist1.specification()[0]), &(atlist1[0].specification()) ); // NB: specification of individual attribute IS an element of the ALS!
    spec->extend<unsigned int>( "i" );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS size (2)", spec->size(), 4UL );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL1 size (2)", atlist1.size(), 3UL ); // ok: AL1's spec changed but AL1 did not
    CPPUNIT_ASSERT_MESSAGE( "AL1 spec (2)", &(atlist1.specification()) != (const AttributeListSpecification*)spec ); // FIX CORALCOOL-2904: change AL1's spec so that it represents AL1 (no longer shared, spec count=1)
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL1 spec size (2)", atlist1.specification().size(), 3UL ); // FIX CORALCOOL-2904
    // AL2 (from extended ALS)
    coral::AttributeList atlist2( *spec, true ); // shared=true, spec count=2
    atlist2.begin()->data<double>() = 10;
    atlist2["y"].data<float>() = 20;
    atlist2[2].data<int>() = 30;
    atlist2[3].data<unsigned int>() = 40;
    spec->release(); // spec count=1 (spec is kept alive by atList2!)
    atlist1.toOutputStream( std::cout );
    std::cout << std::endl;
    atlist2.toOutputStream( std::cout );
    std::cout << std::endl;
    atlist2["y"].setNull();
    atlist2.toOutputStream( std::cout );
    std::cout << std::endl;
    // AL3 (from AL2 from extended ALS)
    coral::AttributeList atlist3 = atlist2; // shared too, spec count=2
    atlist3.toOutputStream( std::cout );
    std::cout << std::endl;
    // AL4 (from AL2 from extended ALS)
    coral::AttributeList atlist4 = atlist3; // shared too, spec count=3
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL4 size (2)", atlist4.size(), 4UL );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL4 spec size (2)", atlist4.specification().size(), 4UL );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL4 spec (2)", &(atlist4.specification()), (const AttributeListSpecification*)spec );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL4[3] name (2)", atlist4[3].specification().name(), std::string("i") );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL4 spec[3] name (2)", atlist4.specification()[3].name(), std::string("i") );
    atlist4.extend< unsigned int >( "ii" ); // no longer shared, spec count=2!
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL4 size (3)", atlist4.size(), 5UL );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL4[3] name (3)", atlist4[3].specification().name(), std::string("i") );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL4 spec[3] name (3)", atlist4.specification()[3].name(), std::string("i") );
    CPPUNIT_ASSERT_MESSAGE( "AL4 spec (3)", &(atlist4.specification()) != (const AttributeListSpecification*)spec );
    atlist4[ "ii" ].data< unsigned int >() = 44;
    atlist4.toOutputStream( std::cout );
    // BEGIN WEIRDNESS TESTS
    {
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL1 size (3)", atlist1.size(), 3UL );
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL1 spec size (3)", atlist1.specification().size(), 3UL ); // OK fixed as seen
      // AL5 (from AL1, whose spec was no longer representing it...)
      coral::AttributeList atlist5 = atlist1; // shared too, spec count=4
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL5 size (3)", atlist5.size(), 3UL );
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL5 spec size (3)", atlist5.specification().size(), 3UL ); // OK fixed as seen
      CPPUNIT_ASSERT_MESSAGE( "AL5 spec (3)", &(atlist5.specification()) != (const AttributeListSpecification*)spec ); // OK fixed as seen
      CPPUNIT_ASSERT_THROW_MESSAGE( "AL5[3] name (3)", atlist5[3]/*.specification().name()*/, coral::Exception );
      CPPUNIT_ASSERT_THROW_MESSAGE( "AL5 spec[3] name (3)", atlist5.specification()[3]/*.name()*/, coral::Exception ); // changed and better after the fix
      std::cout << std::endl << "AL5 (before extending AL5)" << std::endl;
      atlist5.toOutputStream( std::cout );
      std::cout << std::endl << "AL5 spec (before extending AL5)" << std::endl;
      coral::AttributeList(atlist5.specification()).toOutputStream( std::cout );
      atlist5.extend< unsigned int >( "ii" );
      std::cout << std::endl << "AL5 (after extending AL5 with ii)" << std::endl;
      atlist5.toOutputStream( std::cout ); // OK this now prints 4 elements
      std::cout << std::endl << "AL5 spec (after extending AL5 with ii)" << std::endl;
      coral::AttributeList(atlist5.specification()).toOutputStream( std::cout );
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL5 size (4)", atlist5.size(), 4UL );
      CPPUNIT_ASSERT_MESSAGE( "AL5 spec (4)", &(atlist5.specification()) != (const AttributeListSpecification*)spec ); // ok as before the fix
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL5 spec size (4)", atlist5.specification().size(), 4UL ); // FIX CORALCOOL-2904 (was 5UL!)
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL5[3] name (4)", atlist5[3].specification().name(), std::string("ii") ); // FIX CORALCOOL-2904 (was "i"!)
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL5 spec[3] name (4)", atlist5.specification()[3].name(), std::string("ii") ); // FIX CORALCOOL-2904 (was "i"!)
      atlist5[ "ii" ].data< unsigned int >() = 44; // FIX CORALCOOL-2904 (was throwing!)
      CPPUNIT_ASSERT_THROW_MESSAGE( "AL5[i] throws (4)", atlist5["i"], coral::Exception ); // FIX CORALCOOL-2904 (was not throwing!)
      atlist5.toOutputStream( std::cout );
    }
    // END WEIRDNESS TESTS
    std::cout << std::endl;
  }

  // ------------------------------------------------------

  void
  test_AL_atLiWithSharedSpec1() // CORALCOOL-2904
  {
    // ALS
    coral::AttributeListSpecification* spec = new coral::AttributeListSpecification; // spec count=1
    spec->extend<double>( "x" );
    spec->extend( "y", "float" );
    spec->extend( "z", typeid(int) );
    // AL1
    coral::AttributeList atlist1( *spec, true ); // shared=true, spec count=2
    spec->extend<unsigned int>( "i" );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS size (2)", spec->size(), 4UL );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL1 size (2)", atlist1.size(), 3UL );
    // BEGIN WEIRDNESS TESTS
    {
      // AL5 (from AL1, whose spec now no longer represents it...)
      coral::AttributeList atlist5 = atlist1; // shared too, spec count=3
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL5 size (3)", atlist5.size(), 3UL );
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL5 spec size (3)", atlist5.specification().size(), 3UL ); // FIX CORALCOOL-2904
      // AL5 out of scope, spec count=2
    }
    // END WEIRDNESS TESTS
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS size (2)", spec->size(), 4UL );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL1 size (2)", atlist1.size(), 3UL );
    spec->release(); // spec count=1 (kept alive by AL1)
  }

  // ------------------------------------------------------

  void
  test_AL_atLiWithSharedSpec1Count() // CORALCOOL-2904
  {
    // ALS pointers
    coral::AttributeListSpecification* spec;
    coral::AttributeListSpecification* spec5;
    {
      // ALS
      spec = new coral::AttributeListSpecification; // spec count=1
      if ( debug2904 ) std::cout << "__ALS " << spec << " EXPECT: 1" << std::endl;
      spec->extend<double>( "x" );
      spec->extend( "y", "float" );
      spec->extend( "z", typeid(int) );
      // AL1
      coral::AttributeList atlist1( *spec, true ); // shared=true, spec count=2
      if ( debug2904 ) std::cout << "__ALS " << spec << " EXPECT: 2" << std::endl;
      spec->extend<unsigned int>( "i" );
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS size (2)", spec->size(), 4UL );
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL1 size (2)", atlist1.size(), 3UL );
      // BEGIN WEIRDNESS TESTS
      {
        // AL5 (from AL1, whose spec now no longer represents it...)
        coral::AttributeList atlist5 = atlist1; // shared too, spec count=3
        if ( debug2904 ) std::cout << "__ALS " << spec << " EXPECT: 3" << std::endl;
        CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL5 size (3)", atlist5.size(), 3UL );
        CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL5 spec size (3)", atlist5.specification().size(), 3UL ); // FIX CORALCOOL-2904
        spec5 = const_cast<coral::AttributeListSpecification*>(&(atlist5.specification()));
        if ( debug2904 ) std::cout << "__ALS " << spec5 << " EXPECT: 1" << std::endl;
        if ( debug2904 ) std::cout << "__ALS " << spec << " EXPECT: 2" << std::endl;
      }
      if ( debug2904 ) std::cout << "__ALS " << spec5 << " EXPECT: 0" << std::endl;
      // END WEIRDNESS TESTS
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS size (2)", spec->size(), 4UL );
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL1 size (2)", atlist1.size(), 3UL );
      spec->release(); // spec count=1 (kept alive by AL1)
      if ( debug2904 ) std::cout << "__ALS " << spec << " EXPECT: 1" << std::endl;
    }
    if ( debug2904 ) std::cout << "__ALS " << spec << " EXPECT: 0" << std::endl;
  }

  // ------------------------------------------------------

  void
  test_AL_atLiWithSharedSpec2() // CORALCOOL-2904
  {
    std::cout << std::endl;
    // ALS
    coral::AttributeListSpecification* spec = new coral::AttributeListSpecification;
    spec->extend<double>( "x" );
    spec->extend( "y", "float" );
    spec->extend( "z", typeid(int) );
    // AL1
    coral::AttributeList atlist1( *spec, true ); // shared=true, spec count=2
    spec->extend<unsigned int>( "i" );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS size (2)", spec->size(), 4UL );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL1 size (2)", atlist1.size(), 3UL );
    // BEGIN WEIRDNESS TESTS
    {
      std::cout << std::endl << "== 1. before creating AL5";
      std::cout << std::endl << "AL1 (before creating AL5)" << std::endl;
      atlist1.toOutputStream( std::cout );
      std::cout << std::endl << "ALS (before creating AL5)" << std::endl;
      coral::AttributeList(*spec).toOutputStream( std::cout );
      // AL5 (from AL1, whose spec now no longer represents it...)
      coral::AttributeList atlist5 = atlist1; // shared too, spec count=4
      std::cout << std::endl << std::endl << "== 2. before extending AL5";
      std::cout << std::endl << "AL1 (before extending AL5)" << std::endl;
      atlist1.toOutputStream( std::cout );
      std::cout << std::endl << "ALS (before extending AL5)" << std::endl;
      coral::AttributeList(*spec).toOutputStream( std::cout );
      std::cout << std::endl << "AL5 (before extending AL5)" << std::endl;
      atlist5.toOutputStream( std::cout );
      // Extend AL5
      atlist5.extend< unsigned int >( "ii" );
      std::cout << std::endl << std::endl << "== 3. after extending AL5";
      std::cout << std::endl << "AL1 (after extending AL5)" << std::endl;
      atlist1.toOutputStream( std::cout );
      std::cout << std::endl << "ALS (after extending AL5)" << std::endl;
      coral::AttributeList(*spec).toOutputStream( std::cout );
      std::cout << std::endl << "AL5 (after extending AL5)" << std::endl;
      atlist5.toOutputStream( std::cout );
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL5[3] name (4)", atlist5[3].specification().name(), std::string("ii") ); // FIX CORALCOOL-2904 (was "i"!)
    }
    std::cout << std::endl;
    {
      // Same as above, using AL::extend(name,typename) instead of templated
      coral::AttributeList atlist5a = atlist1;
      atlist5a.extend( "ii", "unsigned int" );
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL5a[3] name (4)", atlist5a[3].specification().name(), std::string("ii") ); // FIX CORALCOOL-2904 (was "i"!)
    }
    {
      // Same as above, using AL::extend(name,type) instead of templated
      coral::AttributeList atlist5b = atlist1;
      atlist5b.extend( "ii", typeid(unsigned int) );
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL5b[3] name (4)", atlist5b[3].specification().name(), std::string("ii") ); // FIX CORALCOOL-2904 (was "i"!)
    }
    // END WEIRDNESS TESTS
    spec->release();
  }

  // ------------------------------------------------------

  void
  test_AL_atLiMerge1() // see CORALCOOL-2904
  {
    // ALS
    coral::AttributeListSpecification* spec = new coral::AttributeListSpecification;
    spec->extend<double>( "x" );
    spec->extend( "y", "float" );
    spec->extend( "z", typeid(int) );
    // AL1
    coral::AttributeList atlist1( *spec, true ); // shared=true, spec count=2
    spec->extend<unsigned int>( "i" );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS size (2)", spec->size(), 4UL );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL1 size (2)", atlist1.size(), 3UL );
    // AL6 with a different attribute "j" so that merge would extend ALs
    coral::AttributeList atlist6;
    atlist6.extend<bool>( "j" );
    // Extend AL1 by merging AL6 into it
    atlist1.merge( atlist6 );
    spec->release();
  }

  // ------------------------------------------------------

  void
  test_AL_atLiMerge2() // see CORALCOOL-2904
  {
    // ALS
    coral::AttributeListSpecification* spec = new coral::AttributeListSpecification;
    spec->extend<double>( "x" );
    spec->extend( "y", "float" );
    spec->extend( "z", typeid(int) );
    // AL1
    coral::AttributeList atlist1( *spec, true ); // shared=true, spec count=2
    spec->extend<unsigned int>( "i" );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "ALS size (2)", spec->size(), 4UL );
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL1 size (2)", atlist1.size(), 3UL );
    // AL5 (from AL1, whose spec now no longer represents it...)
    coral::AttributeList atlist5 = atlist1; // shared too, spec count=4
    spec->extend<unsigned int>( "ii" );
    // AL6 with a different attribute "j" so that merge would extend ALs
    coral::AttributeList atlist6;
    atlist6.extend<bool>( "j" );
    // Extend AL5 by merging AL6 into it
    atlist5.merge( atlist6 );
    spec->release();
  }

  // ------------------------------------------------------

  void
  test_AL_atLiMerge3() // see CORALCOOL-2904
  {
    {
      // AL6 with an attribute "j"
      coral::AttributeList atlist6;
      atlist6.extend<bool>( "j" );
      atlist6[ "j" ].data< bool >() = false;
      // Create AL7 as a copy of AL6, then merge AL6 into it again
      // This should share the data, taking that of AL7 instead of that from AL6
      coral::AttributeList atlist7( atlist6 );
      atlist7[ "j" ].data< bool >() = true;
      // Check, merge and check again
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL6[j] (1)", atlist6[ "j" ].data< bool >(), false );
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL7[j] (1)", atlist7[ "j" ].data< bool >(), true );
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL6 size (1)", atlist6.size(), 1UL );
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL7 size (1)", atlist7.size(), 1UL );
      atlist7.merge( atlist6 ); // this should replace j6 by j7
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL6[j] (2)", atlist6[ "j" ].data< bool >(), false ); // unchanged
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL7[j] (2)", atlist7[ "j" ].data< bool >(), false ); // changed!
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL6 size (2)", atlist6.size(), 1UL ); // unchanged
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL7 size (2)", atlist7.size(), 1UL ); // unchanged
    }
    {
      // AL6a with an attribute "j"
      coral::AttributeList atlist6a;
      atlist6a.extend<bool>( "j" );
      atlist6a[ "j" ].data< bool >() = false;
      // AL7a with a different attribute "jj" so that merge would extend ALs
      coral::AttributeList atlist7a;
      atlist7a.extend<bool>( "jj" );
      atlist7a[ "jj" ].data< bool >() = true;
      // Check, merge and check again
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL6a[j] (1)", atlist6a[ "j" ].data< bool >(), false );
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL7a[jj] (1)", atlist7a[ "jj" ].data< bool >(), true );
      CPPUNIT_ASSERT_THROW_MESSAGE( "AL7a[j] (1)", atlist7a[ "j" ], coral::Exception );
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL6a size (1)", atlist6a.size(), 1UL );
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL7a size (1)", atlist7a.size(), 1UL );
      atlist7a.merge( atlist6a ); // this should extend al6 by adding jj
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL6a[j] (1)", atlist6a[ "j" ].data< bool >(), false ); // unchanged
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL7a[jj] (1)", atlist7a[ "jj" ].data< bool >(), true ); // unchanged
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL7a[j] (1)", atlist7a[ "j" ].data< bool >(), false ); // changed!
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL6a size (2)", atlist6a.size(), 1UL ); // unchanged
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL7a size (2)", atlist7a.size(), 2UL ); // changed!
    }
    {
      // AL6b with an attribute "j"
      coral::AttributeList atlist6b;
      atlist6b.extend<bool>( "j" );
      atlist6b[ "j" ].data< bool >() = false;
      // AL7b with an attribute "j" with a different type: merge should fail!
      coral::AttributeList atlist7b;
      atlist7b.extend<float>( "j" );
      CPPUNIT_ASSERT_THROW_MESSAGE( "Merge AL6b into AL7b", atlist7b.merge( atlist6b ), coral::Exception ); // this should fail!
    }
  }

  // ------------------------------------------------------

  void
  test_AL_atLiWithSharedSpecNastyUser() // see also CORALCOOL-2904
  {
    std::cout << std::endl;
    coral::AttributeListSpecification* spec = new coral::AttributeListSpecification;
    if ( debug2904 ) std::cout << "__ALS " << spec << " EXPECT: 1" << std::endl;
    {
      spec->extend<double>( "x" );
      coral::AttributeList atlist( *spec, true );
      if ( debug2904 ) std::cout << "__ALS " << spec << " EXPECT: 2" << std::endl;
      atlist[ "x" ].data< double >() = 1234.;
      spec->release(); // spec instance is kept alive by AL
      if ( debug2904 ) std::cout << "__ALS " << spec << " EXPECT: 1" << std::endl;
      std::cout << "Before nasty user: " << spec << std::endl;
      std::cout << "ALS: ";
      {
        coral::AttributeList dummy(*spec,true);
        if ( debug2904 ) std::cout << "__ALS " << spec << " EXPECT: 2" << std::endl;
        dummy.toOutputStream( std::cout );
      }
      if ( debug2904 ) std::cout << "__ALS " << spec << " EXPECT: 1" << std::endl;
      std::cout << std::endl << "AL:  ";
      atlist.toOutputStream( std::cout );
      std::cout << std::endl;
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL[x] (1)", atlist[ "x" ].data< double >(), (double)1234. );
      // === Nasty user comes in - START
      // NB1: Nasty user knows spec had count=1 before and must have 1 after!
      // NB2: Do _NOT_ call spec->release() before placement new!
      // [placement new reuses the area allocated by the previous full new!]
      // NB3: THE FOLLOWING IS NOT VALID USER CODE IN CORAL... it's just a test
      spec = new(spec) coral::AttributeListSpecification(); // placement new: start from scratch, count=1 again...
      if ( debug2904 ) std::cout << "__ALS " << spec << " EXPECT: 1" << std::endl;
      spec->extend<int>( "i" );
      // === Nasty user comes in - END
      std::cout << "After  nasty user: " << spec << std::endl;
      std::cout << "ALS: ";
      {
        coral::AttributeList dummy(*spec,true);
        if ( debug2904 ) std::cout << "__ALS " << spec << " EXPECT: 2" << std::endl;
        dummy.toOutputStream( std::cout );
      }
      if ( debug2904 ) std::cout << "__ALS " << spec << " EXPECT: 1" << std::endl;
      std::cout << std::endl << "AL:  ";
      atlist.toOutputStream( std::cout );
      std::cout << std::endl;
      try
      {
        CPPUNIT_ASSERT_EQUAL_MESSAGE( "AL[x] (1)", atlist[ "x" ].data< double >(), (double)1234. ); // maybe this will succeed if this issue is fixed...
      }
      catch( coral::AttributeListException& )
      {
        std::cout << "This 'nasty user' code is NOT valid CORAL code: ignore any failure!" << std::endl; // ...but more likely it will fail, no effort should be made to fix this!
      }      
    }
    if ( debug2904 ) std::cout << "__ALS " << spec << " EXPECT: 0" << std::endl; // AL out of scope
  }

  // ------------------------------------------------------

  void
  test_AL_atLiBound()
  {
    std::cout << std::endl;
    class MyObject
    {
    public:
      MyObject() : x(1), y(2), z(3), b( true ) {}
      ~MyObject() {}
      double x;
      float y;
      long z;
      bool b;
    };
    MyObject myObject;
    coral::AttributeList atlist;
    atlist.extend<double>( "x" );
    atlist["x"].bind( myObject.x );
    atlist.extend<float>( "y" );
    atlist["y"].bind( myObject.y );
    atlist.extend<long>( "z" );
    atlist["z"].bindUnsafely( &(myObject.z) );
    atlist.extend<bool>( "b" );
    atlist["b"].bindUnsafely( &(myObject.b) );
    atlist.extend<std::string>( "unchanged" );
    atlist["unchanged"].data<std::string>() = "data";
    atlist.toOutputStream( std::cout );
    std::cout << std::endl;
    myObject.x = 123;
    myObject.y = 231;
    myObject.z = 312;
    myObject.b = false;
    atlist.toOutputStream( std::cout );
    std::cout << std::endl;
    atlist["x"].data<double>() = 0.123;
    std::cout << "Object.x = " << myObject.x << std::endl;
  }

  // ------------------------------------------------------

  void
  test_AL_atLiShared()
  {
    std::cout << std::endl;
    std::cout << "Testing shared lists..." << std::endl;
    coral::AttributeList atlist1;
    atlist1.extend<double>( "x" );
    atlist1["x"].data<double>() = 1;
    atlist1.extend<float>( "y" );
    atlist1["y"].data<float>() = 2;
    atlist1.extend<int>( "z" );
    atlist1["z"].data<int>() = 3;
    coral::AttributeList atlist2;
    atlist2.extend<double>( "XX" );
    atlist2["XX"].data<double>() = 10;
    atlist2.extend<float>( "YY" );
    // Share atlist2.YY with atlist1.y
    atlist1["y"].shareData( atlist2["YY"] );
    atlist1["y"].data<float>() = 2;
    atlist1.toOutputStream( std::cout ) << std::endl;
    atlist2.toOutputStream( std::cout ) << std::endl;
    coral::AttributeList atlist3 = atlist2;
    // Share attlist3.YY with atlist2.YY
    atlist3["YY"].shareData( atlist2["YY"] );
    atlist3["YY"].data<float>() = 123;
    atlist1.toOutputStream( std::cout ) << std::endl;
    atlist2.toOutputStream( std::cout ) << std::endl;
    atlist3.toOutputStream( std::cout ) << std::endl;
    std::cout << "Setting null" << std::endl;
    atlist1["y"].setNull( true );
    atlist1.toOutputStream( std::cout ) << std::endl;
    atlist2.toOutputStream( std::cout ) << std::endl;
    atlist3.toOutputStream( std::cout ) << std::endl;
    std::cout << "Setting not null" << std::endl;
    atlist3["YY"].setNull( false );
    atlist1.toOutputStream( std::cout ) << std::endl;
    atlist2.toOutputStream( std::cout ) << std::endl;
    atlist3.toOutputStream( std::cout ) << std::endl;
  }

  // ------------------------------------------------------

  void
  test_attributeReadWrite()
  {
    std::cout << std::endl;
    coral::AttributeList al;
    al.extend("x", typeid(double));
    coral::Attribute& attribute = al[0];
    attribute.data<double>() = 11.22;
    std::cout << attribute.specification().name() << " ("
              << attribute.specification().typeName() << ") : "
              << attribute.data<double>() << std::endl;
    // Test for throwing exception if the attribute is null
    attribute.setNull();
    try
    {
      attribute.data<double>() = 11.22; // must throw exception
      CPPUNIT_FAIL( "NULL attribute data access did not throw" );
    }
    catch ( coral::AttributeException& ) {}
    try
    {
      const coral::Attribute& cAttr = attribute; // const method must throw too
      cAttr.data<double>();
      CPPUNIT_FAIL( "NULL attribute data access (const) did not throw" );
    }
    catch ( coral::AttributeException& ) {}
    std::cout << std::endl;
  }

  // ------------------------------------------------------

  void
  test_attributeReadWrite_testSharingLoop()
  {
    std::cout << std::endl;
    coral::AttributeList al1;
    al1.extend("x", typeid(double));
    coral::AttributeList al2;
    al2.extend("y", typeid(double));
    coral::AttributeList al3;
    al3.extend("z", typeid(double));
    al1[0].shareData(al2[0]);
    al3[0].shareData(al2[0]);
    al1[0].shareData(al3[0]);
    // This should not crash. If the sharing does not prevent sharing loop,
    // the 3 lines below start an infinite loop of recursive function calls
    // causing segmentation fault.
    al1[0].addressOfData();
    al2[0].addressOfData();
    al3[0].addressOfData();
    // Check if sharing is real
    static const double VAL = 3.5;
    al1[0].data<double>() = VAL;
    if (al1[0].data<double>() != VAL || al2[0].data<double>() != VAL || al3[0].data<double>() != VAL)
    {
      CPPUNIT_FAIL( "Data was not shared ..." );
    }
    std::cout << std::endl;
  }

  // ------------------------------------------------------

};

CPPUNIT_TEST_SUITE_REGISTRATION( coral::CoralBaseTest );

CORALCPPUNITTEST_MAIN( CoralBaseTest )
