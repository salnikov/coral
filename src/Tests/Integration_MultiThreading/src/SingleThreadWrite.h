#ifndef TEST_MTHREAD_SINGLETHREADWRITE_H
#define TEST_MTHREAD_SINGLETHREADWRITE_H 1

#include "Testing.h"

class SingleThreadWrite : public Testing
{

public:

  SingleThreadWrite( const std::string& url, size_t no, unsigned& nErrors );

  virtual ~SingleThreadWrite();

  void operator()();

private:

  size_t m_tableno;
  unsigned& m_nErrors;

};

#endif
