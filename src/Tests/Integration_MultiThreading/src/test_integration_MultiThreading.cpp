#include <cstdio>
#include <iostream>
#include <string>
#include <vector>

#include "CoralBase/Exception.h"
#include "CoralBase/MessageStream.h"
#include "CoralBase/../src/coral_thread_headers.h"
#include "CoralBase/../tests/Common/CoralCppUnitDBTest.h"

#include "RelationalAccess/ITransaction.h"

#include "Testing.h"
#include "SingleThreadRead.h"
#include "SingleThreadWrite.h"
#include "TableCreateThread.h"
#include "TableQueryThread.h"

#define LOG( msg ){ coral::MessageStream myMsg("MTHREAD"); myMsg << coral::Always << msg << coral::MessageStream::endmsg; }

namespace coral
{
  class MultiThreadingTest;
  class MultiThreadingThread;
}

//----------------------------------------------------------------------------

class coral::MultiThreadingThread : public Testing
{

public:

  MultiThreadingThread( const std::string& url ) : Testing( url ) {}

  virtual ~MultiThreadingThread() {}

  void write();

  void read();

};

//----------------------------------------------------------------------------

void
coral::MultiThreadingThread::write()
{
  coral::ISessionProxy& session = getSession();

  session.transaction().start();

  coral::ISchema& schema = session.nominalSchema();

  std::vector< TableCreateThread* > threadBodies;

  for ( int i = 0; i < 10; ++i )
    threadBodies.push_back( new TableCreateThread( schema, i, T1 ) );

  std::vector< coral::thread* > threads;
  for ( int i = 0; i < 10; ++i )
    threads.push_back( new coral::thread( *( threadBodies[i] ) ) );

  for ( int i = 0; i < 10; ++i )
    threads[i]->join();

  for ( int i = 0; i < 10; ++i ) {
    delete threads[i];
    delete threadBodies[i];
  }

  coral::sleepSeconds( 1 );

  session.transaction().commit();
}

void
coral::MultiThreadingThread::read()
{
  coral::ISessionProxy& session = getSession();

  session.transaction().start( true );

  coral::ISchema& schema = session.nominalSchema();

  std::vector< TableQueryThread* > threadBodies;

  for ( int i = 0; i < 10; ++i )
    threadBodies.push_back( new TableQueryThread( schema, i, T1 ) );

  std::vector< coral::thread* > threads;
  for ( int i = 0; i < 10; ++i )
    threads.push_back( new coral::thread( *( threadBodies[i] ) ) );

  for ( int i = 0; i < 10; ++i )
    threads[i]->join();

  for ( int i = 0; i < 10; ++i ) {
    delete threads[i];
    delete threadBodies[i];
  }

  session.transaction().commit();
}

//----------------------------------------------------------------------------

class coral::MultiThreadingTest : public coral::CoralCppUnitDBTest
{
  CPPUNIT_TEST_SUITE( MultiThreadingTest );
  CPPUNIT_TEST( test_multiThreading );
  CPPUNIT_TEST_SUITE_END();

public:

  void test_multiThreading();
  
  MultiThreadingTest(){}
  virtual ~MultiThreadingTest(){}

};

//----------------------------------------------------------------------------

void coral::MultiThreadingTest::test_multiThreading()
{
  Testing::T1 = BuildUniqueTableName( "MTTEST_T1" );
  Testing::T2 = BuildUniqueTableName( "MTTEST_T2" );
  Testing::T3 = BuildUniqueTableName( "MTTEST_T3" );
  Testing::V0 = BuildUniqueTableName( "MTTEST_V0" );

  /* start with the tests *///
  MultiThreadingThread thread( UrlRW() );
  LOG("Starting[write] one session with multithreaded requests");
  thread.createSession(0);
  thread.write();
  thread.deleteSession();
  LOG("Starting[read] one session with multithreaded requests");
  thread.createSession(0, coral::ReadOnly);
  thread.read();
  thread.deleteSession();
  unsigned nErrors = 0;
  // multithreaded sessions write
  LOG("Starting[write] one session per thread");
  {
    std::vector< SingleThreadWrite* > threadBodies;
    for ( int i = 0; i < 10; ++i )
    {
      SingleThreadWrite* swrite = new SingleThreadWrite( UrlRW(), i, nErrors );
      threadBodies.push_back( swrite );
    }
    std::vector< coral::thread* > threads;
    for ( int i = 0; i < 10; ++i )
      threads.push_back( new coral::thread( *( threadBodies[i] ) ) );
    for ( int i = 0; i < 10; ++i )
      threads[i]->join();
    for ( int i = 0; i < 10; ++i )
    {
      delete threads[i];
      SingleThreadWrite* swrite = threadBodies[i];
      delete swrite;
    }
  }
  CPPUNIT_ASSERT_EQUAL_MESSAGE( "#errors (write threads)", 0u, nErrors );
  // multithreaded sessions read
  LOG("Starting[read] one session per thread");
  {
    std::vector< SingleThreadRead* > threadBodies;
    for ( int i = 0; i < 10; ++i )
    {
      SingleThreadRead* sread = new SingleThreadRead( UrlRO(), i, nErrors );
      threadBodies.push_back( sread );
    }
    std::vector< coral::thread* > threads;
    for ( int i = 0; i < 10; ++i )
      threads.push_back( new coral::thread( *( threadBodies[i] ) ) );
    for ( int i = 0; i < 10; ++i )
      threads[i]->join();
    for ( int i = 0; i < 10; ++i )
    {
      delete threads[i];
      SingleThreadRead* sread = threadBodies[i];
      delete sread;
    }
  }
  CPPUNIT_ASSERT_EQUAL_MESSAGE( "#errors (read threads)", 0u, nErrors );
}

//----------------------------------------------------------------------------

CPPUNIT_TEST_SUITE_REGISTRATION( coral::MultiThreadingTest );

CORALCPPUNITTEST_MAIN( MultiThreadingTest )
