#! /bin/bash
topdir=`dirname $0`
topdir=`cd $topdir; pwd`

# Check optional arguments: usage if 1st argument is --help
if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
  echo "Usage: `basename $0` [--valgrind] [tests]          --> Run tests"
  echo "Usage: `basename $0` -ls|--list [tests]            --> List available tests"
  echo "Usage: `basename $0` -h|--help                     --> Print usage"
  exit 1
fi

# Check optional arguments: list tests if 1st argument is --list
listonly=0
if [ "$1" == "-ls" ] || [ "$1" == "--list" ]; then
  listonly=1
  shift
# Check optional arguments: run through valgrind if 1st argument is --valgrind
elif [ "$1" == "-valgrind" ] || [ "$1" == "--valgrind" ]; then
  echo "The test suite will be executed through valgrind ('-valgrind')"
  export CORAL_TESTSUITE_VALGRIND=1
  shift
else
  echo "The test suite will NOT be executed through valgrind (no '-valgrind')"
  unset CORAL_TESTSUITE_VALGRIND
fi

# Check next optional arguments to choose which tests to run
if [ "$1" = "" ]; then
  #--- All tests (whether in CoralTest or CoolTest)
  theTests=ALL 
  #--- Specific tests (only in CoralTest)
  ###theTests=integration
  ###theTests=unit
  ###theTests=unit_coralserver
  #--- Specific tests (only in CoolTest)
  ###theTests=sqlite
  ###theTests=relationalcool.oracle.raldatabasesvc
  ###theTests=relationalcool.mysql.raldatabasesvc
  ###theTests=relationalcool.sqlite.raldatabasesvc
  ###theTests=pycool.import_pycool
  ###theTests=pycoolutilities.sqlite.regression
  ###theTests=pycoolutilities.frontier
else
  theTests=""
  while [ "$1" != "" ]; do
    if [ "$theTests" = "" ]; then
      theTests="$1"
    else
      theTests="$theTests $1"
    fi
    shift
  done
fi

# Check if this is CORAL or COOL
thisProj=@CMAKE_PROJECT_NAME@ # From CMAKE_PROJECT_NAME
if [ "$thisProj" == "CORAL" ]; then
  thisPkg=CoralTest
elif [ "$thisProj" == "COOL" ]; then
  thisPkg=CoolTest
else
  echo "ERROR! Unknown project ${thisProj} is neither CORAL nor COOL"
  exit 1
fi

# Check if qmtest exists under CoralTest or CoolTest
theQmtDir=$topdir/${thisPkg}/qmtest
if [ ! -d $theQmtDir ]; then
  echo "ERROR! Directory ${theQmtDir} does not exist"
  exit 1
fi

# Assume you are in the install area or in the build area
ccrun=$topdir/cc-run
if [ ! -f ${ccrun} ]; then 
  echo "ERROR! ${ccrun} does not exist"
  exit 1
fi
export BINARY_TAG=`${ccrun} printenv BINARY_TAG`

# List tests and exit if --list was specified
if [ "$listonly" == "1" ]; then
  cd $theQmtDir
  ${ccrun} -q qmtest ls -R ${theTests}
  exit 0
fi
echo Will launch \'qmtest run ${theTests}\'

# Define location of the test workdir and of the logs directory
# [TODO? Accept workdir and/or logdir as arguments? Probably no longer needed]
if [ -d ${topdir}/../src ]; then
  # This is most likely a private build directory
  export CORALCOOL_QMTEST_WORKDIR=${theQmtDir}/tmp.${BINARY_TAG}
  theLogParentDir=`cd ${topdir}/..; pwd`
else
  # This is most likely a read-only installed release
  # Use a workdir below /tmp named after the last 3 levels of this release area
  tmpdir=`cd ${topdir}/..; pwd`
  if [ "${tmpdir}" == "/" ]; then
    tmpdir=
  elif [ "${tmpdir%/*/*/*}" != "${tmpdir}" ]; then
    tmpdir=${tmpdir#${tmpdir%/*/*/*}}
  fi
  export CORALCOOL_QMTEST_WORKDIR=/tmp/${USER}${tmpdir}/tmp.${BINARY_TAG}
  theLogParentDir=/tmp/${USER}${tmpdir}
  if [ ! -d ${theLogParentDir} ]; then mkdir -p ${theLogParentDir}; fi
fi

# Check out from SVN the logs directory if it does not exist
# Stop if an older version of logs exist without qmtestCoral or qmtestCool
# Define the qmtest results file
cd $theLogParentDir
if [ "$?" != "0" ]; then
  echo "ERROR! Could not cd $theLogParentDir"
  exit 1
fi
if [ "$thisPkg" == "CoralTest" ]; then
  if [ ! -d logs ]; then
    echo "WARNING! Missing logs directory ${theLogParentDir}/logs will be checked out from lcgcoral SVN"
    klist > /dev/null 2>&1
    if [ "$?" == "0" ]; then
      svn co -q svn+ssh://svn.cern.ch/reps/lcgcoral/logs
    else
      svn co -q http://svn.cern.ch/guest/lcgcoral/logs
    fi
  fi
  if [ ! -d logs/qmtestCoral ]; then
    echo "ERROR! Directory ${theLogParentDir}/logs/qmtestCoral does not exist"
    exit 1
  fi
  theQmrDir=${theLogParentDir}/logs/qmtestCoral
else
  if [ ! -d logs ]; then
    echo "WARNING! Missing logs directory ${theLogParentDir}/logs will be checked out from lcgcool SVN"
    klist > /dev/null 2>&1
    if [ "$?" == "0" ]; then
      svn co -q svn+ssh://svn.cern.ch/reps/lcgcool/logs
    else
      svn co -q http://svn.cern.ch/guest/lcgcool/logs
    fi
  fi
  if [ ! -d logs/qmtestCool ]; then
    echo "ERROR! Directory ${theLogParentDir}/logs/qmtestCool does not exist"
    exit 1
  fi
  theQmrDir=${theLogParentDir}/logs/qmtestCool
fi

# Set a few additional environment variables
# (as in prepare_env in test_functions.sh)
###export CORAL_AUTH_PATH=${HOME}/private
###export CORAL_DBLOOKUP_PATH=${HOME}/private

# Ignore QMTEST timeouts for COOL
export COOL_IGNORE_TIMEOUT=yes

# Check that QMTEST_CLASS_PATH contains the qmtest directory (bug #86964)
qmtest_class_path=`${ccrun} printenv QMTEST_CLASS_PATH`
echo "Using QMTEST_CLASS_PATH=$qmtest_class_path"
if [[ :$qmtest_class_path: != *:${theQmtDir}:* ]] ; then
  theQmtDirP=`cd ${theQmtDir}; pwd -P` # See CORALCOOL-2876
  if [[ :$qmtest_class_path: != *:${theQmtDirP}:* ]] ; then
    echo "ERROR! Expected QMTEST_CLASS_PATH should contain ${theQmtDir} or ${theQmtDirP}"
    exit 1
  fi
fi

# Check that valgrind is available if required
if [ "$CORAL_TESTSUITE_VALGRIND" == "1" ]; then
  ${ccrun} which valgrind > /dev/null 2>&1
  if [ "$?" != "0" ]; then
    echo "ERROR! No path to valgrind"
    echo "PATH is:$PATH" | tr ":" "\n"
    exit 1
  fi
  # Fix "valgrind: failed to start tool 'memcheck' for platform 'amd64-linux'"
  # relocatability issues by setting VALGRIND_LIB (thanks to Rolf!)
  # [NB already set via CMT/CMake; redundantly set here for native valgrind use]
  binvalgrind=`${ccrun} which valgrind`
  binvalgrind=`dirname $binvalgrind`
  if [ ! -d $binvalgrind/../lib/valgrind ]; then
    echo "ERROR! Directory $binvalgrind/../lib/valgrind does not exist!"
    exit 1
  fi
  VALGRIND_LIB=`cd $binvalgrind/../lib/valgrind; pwd`
  # Set the path to the suppression file (no longer set via CMT!)
  ${ccrun} which coralValgrindWrapper.sh > /dev/null 2>&1
  if [ "$?" != "0" ]; then
    echo "ERROR! No path to coralValgrindWrapper.sh"
    echo "PATH is:$PATH" | tr ":" "\n"
    exit 1
  fi
  CORAL_TESTSUITE_VALGRIND_SUPP=`${ccrun} which coralValgrindWrapper.sh`
  CORAL_TESTSUITE_VALGRIND_SUPP=`dirname ${CORAL_TESTSUITE_VALGRIND_SUPP}`
  CORAL_TESTSUITE_VALGRIND_SUPP=`cd ${CORAL_TESTSUITE_VALGRIND_SUPP}; pwd`/valgrind.supp
  if [ ! -f "${CORAL_TESTSUITE_VALGRIND_SUPP}" ]; then
    echo "ERROR! Suppression file not found: ${CORAL_TESTSUITE_VALGRIND_SUPP}" 
    exit 1
  fi
  CORAL_TESTSUITE_VALGRIND_LOGDIR=${theQmrDir}/valgrind # No trailing '/'
fi

# Run the tests from the qmtest directory
echo "Using CORALCOOL_QMTEST_WORKDIR=${CORALCOOL_QMTEST_WORKDIR}"
cd $theQmtDir
if [ "${BINARY_TAG}" == "" ]; then
  echo "ERROR! BINARY_TAG is not set"
  exit 1
fi
theQmr=${theQmrDir}/${BINARY_TAG}.qmr # Drop "-cmake" (CORALCOOL-2842)
echo "Launch tests - results will be in ${theQmr}"
qmtestRun="qmtest run"
qmtestSum="qmtest summarize"
if [ `${ccrun} python -c 'import platform; print (platform.system())'` == "Darwin" ]; then qmtestRun="python `${ccrun} which qmtest` run"; qmtestSum="python `${ccrun} which qmtest` summarize"; fi # CORALCOOL-2884 and CORALCOOL-2883
echo Execute \'${ccrun} ${qmtestRun} ${theTests}\'
${ccrun} -q VALGRIND_LIB=$VALGRIND_LIB CORAL_TESTSUITE_VALGRIND_SUPP=$CORAL_TESTSUITE_VALGRIND_SUPP CORAL_TESTSUITE_VALGRIND_LOGDIR=$CORAL_TESTSUITE_VALGRIND_LOGDIR ${qmtestRun} -o ${theQmr} ${theTests} > /dev/null # NB Extra env are needed (and more explicit than "export")!
${ccrun} -q ${qmtestSum} -f stats ${theQmr} ${theTests} | awk 'BEGIN{out=0}{if ($2=="STATISTICS") out=1; if (out==1) print}'

# Beautify and move the valgrind logs
if [ "$CORAL_TESTSUITE_VALGRIND" == "1" ]; then
  cd $theQmrDir/valgrind
  valfiles=`ls $BINARY_TAG/valgrind.*`
  if [ "$valfiles" == "" ]; then
    echo "WARNING! No valgrind logs found in $theQmrDir/valgrind/$BINARY_TAG/"
  else
    for valfile in $valfiles; do
      ###echo $valfile
      ${theQmtDir}/sedValgrindLog.sh $valfile
      mv $valfile .
    done
  fi
fi

# Do not attempt to run 'qmtest report/summarize' as "$?" is wrong on OSX?
