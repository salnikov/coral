#
# This file is mainly intended as a CORAL/COOL workaround for using, against
# Python2, an installation of QMTest built for Python3 (CORALCOOL-2954).
# The QMTest module qm.sigmask on Python2 comes from the sigmask.so library,
# built from the sigmask.c C file; on Python 3, the module cannot be found
# because the library is called sigmask.cpython-35m-x86_64-linux-gnu.so.
# As a workaround, on Python3, qm.sigmask is overloaded from a simple python
# module providing a no-op implementation of sigmask (which is not needed).
#

# No-op implementation of QMTest qm.sigmask.save_mask (see sigmask.c) 
def save_mask():
    pass

# No-op implementation of QMTest qm.sigmask.restore_mask (see sigmask.c) 
def restore_mask():
    pass
