#include "CoralKernel/Property.h"

using namespace coral;

Property::Property() :
  m_lastID(0), m_mutex()
{
  //std::cout << "Create Property " << this << std::endl;
}

Property::~Property()
{
  //std::cout << "Delete Property " << this << std::endl;
}

bool Property::set(const std::string& value)
{
  {
    // MONITOR_START_CRITICAL
    std::lock_guard<std::mutex> lock( m_mutex );
    m_value = value;
    for( std::map<CallbackID, Callback>::iterator i = m_callbacks.begin();
         i != m_callbacks.end(); ++i ) i->second( m_value );
    return true;
    // MONITOR_END_CRITICAL
  }
}

const std::string& Property::get() const
{
  {
    // MONITOR_START_CRITICAL
    std::lock_guard<std::mutex> lock( m_mutex );
    return m_value;
    // MONITOR_END_CRITICAL
  }
}


Property::CallbackID Property::registerCallback(Property::Callback& cb)
{
  {
    // MONITOR_START_CRITICAL
    std::lock_guard<std::mutex> lock( m_mutex );
    m_callbacks[m_lastID] = cb;
    return m_lastID++;
    // MONITOR_END_CRITICAL
  }
}

bool Property::unregisterCallback(Property::CallbackID& id)
{
  {
    // MONITOR_START_CRITICAL
    std::lock_guard<std::mutex> lock( m_mutex );
    return m_callbacks.erase(id) > 0;
    // MONITOR_END_CRITICAL
  }
}
