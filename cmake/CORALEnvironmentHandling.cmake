# Enable environment handling in CMake projects.
#
# Usage
# -----
#
#

macro(coral_enable_env)

  set(env_xml_basename ${CMAKE_PROJECT_NAME}.xenv
      CACHE STRING "basename of the XML environment file")

  set(env_build_xml ${CMAKE_BINARY_DIR}/env/${env_xml_basename}
      CACHE STRING "full path to the XML file for the environment to be used in building and testing")

  set(env_release_xml ${CMAKE_BINARY_DIR}/TMP.install/env/${env_xml_basename}
      CACHE STRING "full path to the XML file for the environment to be used once the project is installed")

  mark_as_advanced(env_xml_basename)

  if (LCG_python3 STREQUAL on) # CORALCOOL-2976
    message(STATUS "Looking for python3")
    find_package(PythonInterp REQUIRED 3) # Needed to define PYTHON_EXECUTABLE
  else()
    message(STATUS "Looking for python2")
    find_package(PythonInterp REQUIRED) # Needed to define PYTHON_EXECUTABLE
  endif()

endmacro()

#-------------------------------------------------------------------------------
# coral_release_env(<DEFAULT|SET|PREPEND|APPEND|REMOVE|UNSET|INCLUDE|TARGET|PACKAGE> <var> <value> [...repeat...])
#
# Declare environment variables to be modified.
# Note: this is just a wrapper around set_property, the actual logic is in
# coral_generate_env_conf().
#-------------------------------------------------------------------------------
function(coral_release_env)
  #message(STATUS "coral_release_env(): ARGN -> ${ARGN}")
  # ensure that the variables in the value are not expanded when passing the arguments
  #string(REPLACE "\$" "\\\$" _argn "${ARGN}")
  #message(STATUS "_argn -> ${_argn}")
  set_property(GLOBAL APPEND PROPERTY CORAL_RELEASE_ENVIRONMENT "${ARGN}")
endfunction()

#-------------------------------------------------------------------------------
# coral_build_env(<DEFAULT|SET|PREPEND|APPEND|REMOVE|UNSET|INCLUDE> <var> <value> [...repeat...])
#
# Same as coral_release_env(), but the environment is set only for building.
#-------------------------------------------------------------------------------
function(coral_build_env)
  #message(STATUS "coral_build_env(): ARGN -> ${ARGN}")
  # ensure that the variables in the value are not expanded when passing the arguments
  #string(REPLACE "\$" "\\\$" _argn "${ARGN}")
  #message(STATUS "_argn -> ${_argn}")
  set_property(GLOBAL APPEND PROPERTY CORAL_BUILD_ENVIRONMENT "${ARGN}")
endfunction()

#-------------------------------------------------------------------------------
# coral_build_and_release_env(<DEFAULT|SET|PREPEND|APPEND|REMOVE|UNSET|INCLUDE> <var> <value> [...repeat...])
#
# Same as coral_release_env(), but the environment is set also for building.
#-------------------------------------------------------------------------------
function(coral_build_and_release_env)
  #message(STATUS "coral_build_and_release_env(): ARGN -> ${ARGN}")
  # ensure that the variables in the value are not expanded when passing the arguments
  #string(REPLACE "\$" "\\\$" _argn "${ARGN}")
  #message(STATUS "_argn -> ${_argn}")
  set_property(GLOBAL APPEND PROPERTY CORAL_BUILD_ENVIRONMENT "${ARGN}")
  set_property(GLOBAL APPEND PROPERTY CORAL_RELEASE_ENVIRONMENT "${ARGN}")
endfunction()

#-------------------------------------------------------------------------------
# _env_conf_pop_instruction(...)
#
# Helper macro used by coral_generate_env_conf.
#-------------------------------------------------------------------------------
macro(_env_conf_pop_instruction instr lst)
  list(LENGTH ${lst} _lst_len0) # FIX CORALCOOL-2793
  #message(STATUS "_env_conf_pop_instruction ${lst} => ${${lst}}")
  list(GET ${lst} 0 ${instr})
  if(${instr} MATCHES "^(INCLUDE|UNSET|TARGET|PACKAGE)$")
    list(GET ${lst} 0 1 ${instr})
    list(REMOVE_AT ${lst} 0 1)
    # even if the command expects only one argument, ${instr} must have 3 elements
    # because of the way it must be passed to _env_line()
    set(${instr} ${${instr}} _dummy_)
  else()
    list(GET ${lst} 0 1 2 ${instr})
    list(REMOVE_AT ${lst} 0 1 2)
  endif()
  #message(STATUS "_env_conf_pop_instruction ${instr} => ${${instr}}")
  #message(STATUS "_env_conf_pop_instruction ${lst} => ${${lst}}")
  list(LENGTH ${lst} _lst_len1) # FIX CORALCOOL-2793
  if(${_lst_len1} STREQUAL ${_lst_len0}) # FIX CORALCOOL-2793
    message(FATAL_ERROR "_env_conf_pop_instruction ${lst} (${${lst}}) ${instr} (${${instr}}) failed")
  endif()
endmacro()

#-------------------------------------------------------------------------------
# _env_package(...)
#
# Helper macro used by _env_line.
#-------------------------------------------------------------------------------
function(_env_package pack output)
  message(STATUS "Requested env for PACKAGE ${pack}")
  # this is needed to get the non-cache variables for the packages
  find_package(${pack} QUIET)

  set(_env)

  if(pack STREQUAL PythonInterp OR pack STREQUAL PythonLibs)
    set(pack Python)
  endif()
  string(TOUPPER ${pack} _pack_upper)

  if(${_pack_upper}_EXECUTABLE AND NOT ${_pack_upper}_BINARY_PATH)
    get_filename_component(${_pack_upper}_BINARY_PATH ${${_pack_upper}_EXECUTABLE} PATH)
  endif()

  if(${_pack_upper}_BINARY_PATH)
    get_filename_component(prefix_path ${${_pack_upper}_BINARY_PATH} PATH)
    find_path(${_pack_upper}_MANPATH NAMES man1 man3 HINTS ${prefix_path}/man ${prefix_path}/share/man NO_DEFAULT_PATH)
    if(${_pack_upper}_MANPATH)
      list(APPEND _env PREPEND MANPATH ${${_pack_upper}_MANPATH})
    endif()
  endif()

  if(${_pack_upper}_INCLUDE_DIR)
    get_filename_component(prefix_path ${${_pack_upper}_INCLUDE_DIR} PATH)
    find_path(${_pack_upper}_MANPATH NAMES man1 man3 HINTS ${prefix_path}/man ${prefix_path}/share/man NO_DEFAULT_PATH)
    if(${_pack_upper}_MANPATH)
      list(APPEND _env PREPEND MANPATH ${${_pack_upper}_MANPATH})
    endif()
  endif()

  if(${CMAKE_PROJECT_NAME} STREQUAL COOL)
    if(${pack}_INCLUDE_DIR AND NOT pack STREQUAL ROOT)
      list(APPEND _env PREPEND ROOT_INCLUDE_PATH ${${pack}_INCLUDE_DIR})
    endif()
    if(${_pack_upper}_INCLUDE_DIR AND NOT pack STREQUAL ROOT)
      list(APPEND _env PREPEND ROOT_INCLUDE_PATH ${${_pack_upper}_INCLUDE_DIR})
    endif()
  endif()

  set(binary_path   ${${pack}_BINARY_PATH} ${${_pack_upper}_BINARY_PATH})
  set(python_path   ${${pack}_PYTHON_PATH} ${${_pack_upper}_PYTHON_PATH})
  set(environment   ${${pack}_ENVIRONMENT} ${${_pack_upper}_ENVIRONMENT})
  set(library_path2 ${${pack}_LIBRARY_DIR} ${${pack}_LIBRARY_DIRS}
                    ${${_pack_upper}_LIBRARY_DIR} 
                    ${${_pack_upper}_LIBRARY_DIRS})

  # Add _EXECUTABLE path to PATH for all packages including Python
  if(${_pack_upper}_EXECUTABLE)
    get_filename_component(bin_path ${${_pack_upper}_EXECUTABLE} PATH)
    ###list(APPEND _env PREPEND PATH ${bin_path}) # Not ok for sqlite on Mac
    set(binary_path ${bin_path} ${binary_path}) # Fix CORALCOOL-2926 on Mac
  endif()

  # Use also the _LIBRARIES variable
  foreach(_lib ${${pack}_LIBRARIES} ${${_pack_upper}_LIBRARIES})
    if(EXISTS ${_lib})
      get_filename_component(_lib ${_lib} PATH)
      list(APPEND library_path2 ${_lib})
    endif()
  endforeach()

  # Concatenate the two library paths (?)
  set(library_path ${library_path} ${library_path2})

  # Remove system directories from the library_path
  ###set(library_path ${library_path} /lib /lib32 /lib64 /usr/lib /usr/lib32 /usr/lib64 /usr/local/lib /usr/local/lib32 /usr/local/lib64) # FOR DEBUGGING ONLY
  set(old_library_path ${library_path})
  set(library_path)
  foreach(d ${old_library_path})
    if(NOT d MATCHES "^(/usr|/usr/local)?/lib(32|64)?")
      set(library_path ${library_path} ${d})
    endif()
  endforeach()

  # Remove system directories from the binary_path (CORALCOOL-2926 on Mac 10.11)
  ###set(binary_path ${binary_path} /bin /sbin /usr/bin /usr/sbin /usr/local/bin /usr/local/sbin) # FOR DEBUGGING ONLY
  set(old_binary_path ${binary_path})
  set(binary_path)
  foreach(d ${old_binary_path})
    if(NOT d MATCHES "^(/usr|/usr/local)?/(s)?bin")
      ###message(STATUS "NOMATCH ${d}") # debug CORALCOOL-2926
      set(binary_path ${binary_path} ${d})
    else()
      ###message(STATUS "MATCH ${d}") # debug CORALCOOL-2926
    endif()
  endforeach()

  foreach(var library_path python_path binary_path)
    if(${var})
      list(REMOVE_DUPLICATES ${var})
    endif()
  endforeach()

  foreach(val ${python_path})
    list(APPEND _env PREPEND PYTHONPATH ${val})
  endforeach()

  foreach(val ${binary_path})
    list(APPEND _env PREPEND PATH ${val}) # All packages except Python
  endforeach()

  foreach(val ${library_path})
    list(APPEND _env PREPEND LD_LIBRARY_PATH ${val})
  endforeach()

  list(APPEND _env ${environment})

  set(data)
  while(_env)
    _env_conf_pop_instruction(instr _env)
    # ensure that the variables in the value are not expanded when passing the arguments
    string(REPLACE "\$" "\\\$" instr "${instr}")
    _env_line(${instr} ln)
    if(ln)
      if(data)
        set(data "${data}\n  ${ln}")
      else()
        set(data "${data}${ln}")
      endif()
    endif()
  endwhile()

  set(${output} "${data}" PARENT_SCOPE)
endfunction()

#-------------------------------------------------------------------------------
# _env_target(...)
#
# Helper macro used by _env_line.
#-------------------------------------------------------------------------------
function(_env_target tgt output)
  message(STATUS "Requested env for TARGET ${tgt}")
  get_target_property(_libs ${tgt} CMAKE_LINK_INTERFACE_LIBRARIES)
  message(STATUS "${tgt}[CMAKE_LINK_INTERFACE_LIBRARIES] -> ${_libs}")

  set(_env)

  set(data)
  while(_env)
    _env_conf_pop_instruction(instr _env)
    # ensure that the variables in the value are not expanded when passing the arguments
    string(REPLACE "\$" "\\\$" instr "${instr}")
    _env_line(${instr} ln)
    if(ln)
      if(data)
        set(data "${data}\n  ${ln}")
      else()
        set(data "${data}${ln}")
      endif()
    endif()
  endwhile()

  set(${output} "${data}" PARENT_SCOPE)
endfunction()

#-------------------------------------------------------------------------------
# _root_vars_replace(...)
#
# Helper macro used by _env_line (used in coral_generate_env_conf).
# This is now also used to set pythonExecutable in cc-run (CORALCOOL-2873).
#-------------------------------------------------------------------------------
macro(_root_vars_replace val output)
  set(val_ ${val})
  foreach(root_var ${root_vars})
    if(${root_var})
      if(val_ MATCHES "^${${root_var}}")
        file(RELATIVE_PATH val_ ${${root_var}} ${val_})
        set(val_ \${${root_var}}/${val_})
      endif()
    endif()
  endforeach()
  #message(STATUS "_root_vars_replace(): ${val} -> ${val_}")
  set (${output} ${val_})
endmacro()

#-------------------------------------------------------------------------------
# _env_line(...)
#
# Helper macro used by coral_generate_env_conf.
#-------------------------------------------------------------------------------
macro(_env_line cmd var val output)
  _root_vars_replace (${val} val_)
  if("${cmd}_" STREQUAL "DEFAULT_")
    set(${output} "<env:default variable=\"${var}\">${val_}</env:default>")
  elseif("${cmd}_" STREQUAL "SET_")
    set(${output} "<env:set variable=\"${var}\">${val_}</env:set>")
  elseif("${cmd}_" STREQUAL "UNSET_")
    set(${output} "<env:unset variable=\"${var}\"><env:unset>")
  elseif("${cmd}_" STREQUAL "PREPEND_")
    set(${output} "<env:prepend variable=\"${var}\">${val_}</env:prepend>")
  elseif("${cmd}_" STREQUAL "APPEND_")
    set(${output} "<env:append variable=\"${var}\">${val_}</env:append>")
  elseif("${cmd}_" STREQUAL "REMOVE_")
    set(${output} "<env:remove variable=\"${var}\">${val_}</env:remove>")
  elseif("${cmd}_" STREQUAL "INCLUDE_")
    get_filename_component(inc_name ${var} NAME)
    get_filename_component(inc_path ${var} PATH)
    set(${output} "<env:include hints=\"${inc_path}\">${inc_name}</env:include>")
  elseif("${cmd}_" STREQUAL "TARGET_")
    _env_target(${var} ${output})
  elseif("${cmd}_" STREQUAL "PACKAGE_")
    _env_package(${var} ${output})
  else()
    message(FATAL_ERROR "Unknown environment command ${cmd}")
  endif()
endmacro()

#-------------------------------------------------------------------------------
# _coral_write_env_conf(filename <env>)
#
# Generate the XML file describing the changes to the environment required by
# this project.
#
# The variable names in ${root_vars} are used to allow relocation of the
# resulting environment.
#-------------------------------------------------------------------------------
include(CMakeParseArguments)
function(_coral_write_env_conf filename)
  cmake_parse_arguments(ARG "" "" "" ${ARGN})

  set(data "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<env:config xmlns:env=\"EnvSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"EnvSchema EnvSchema.xsd \">\n")

  foreach(root_var ${root_vars})
    get_filename_component(_path ${${root_var}} REALPATH) # see CORALCOOL-2836 and CORALCOOL-2876
    set(data "${data} <env:default variable=\"${root_var}\">${_path}</env:default>\n")
  endforeach()

  set(commands ${ARG_UNPARSED_ARGUMENTS})
  #message(STATUS "start - ${commands}")
  while(commands)
    #message(STATUS "iter - ${commands}")
    _env_conf_pop_instruction(instr commands)
    # ensure that the variables in the value are not expanded when passing the arguments
    string(REPLACE "\$" "\\\$" instr "${instr}")
    _env_line(${instr} ln)
    if(ln)
      set(data "${data}  ${ln}\n")
    endif()
  endwhile()
  set(data "${data}</env:config>\n")

  message(STATUS "Generating ${filename}")
  file(WRITE ${filename} "${data}")
endfunction()

#-------------------------------------------------------------------------------
# Generate environment configuration files
# Replace CORALSYS/COOLSYS to ensure relocatability (CORALCOOL-2829)
# Replace LCG_releases_base (if set) to ensure relocatability (CORALCOOL-2829)
#-------------------------------------------------------------------------------
function(coral_generate_env_conf)
  get_property(_build_env GLOBAL PROPERTY CORAL_BUILD_ENVIRONMENT)
  get_property(_release_env GLOBAL PROPERTY CORAL_RELEASE_ENVIRONMENT)
  IF(NOT "${LCG_releases_base} " STREQUAL " ")
    set(root_vars ${CMAKE_PROJECT_NAME}SYS;LCG_releases_base)
  ELSE()
    set(root_vars ${CMAKE_PROJECT_NAME}SYS)
  ENDIF()
  set(${CMAKE_PROJECT_NAME}SYS ${CMAKE_BINARY_DIR}) # local and child scope
  _coral_write_env_conf(${env_build_xml} ${_build_env})
  set(${CMAKE_PROJECT_NAME}SYS ${CMAKE_INSTALL_PREFIX}) # local and child scope
  _coral_write_env_conf(${env_release_xml} ${_release_env})
  _root_vars_replace (${PYTHON_EXECUTABLE} PYTHON_EXECUTABLE_CCPATH)
  string(REPLACE "\$" "" PYTHON_EXECUTABLE_CCPATH ${PYTHON_EXECUTABLE_CCPATH})
  set(PYTHON_EXECUTABLE_CCPATH ${PYTHON_EXECUTABLE_CCPATH} PARENT_SCOPE)
endfunction()
